<div class="content">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Tanggal</th>
        <th>Tipe</th>
        <th>Sumber</th>
        <th>Keterangan</th>
        <th>Jumlah Masuk</th>
        <th>Jumlah Keluar</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($Keuangan as $DataKeuangan)
        <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{HTanggal::FormatDate($DataKeuangan->created_at)}}</td>
          <td>{{$DataKeuangan->TipeText}}</td>
          <td>{{$DataKeuangan->SumberText}}</td>
          <td>{{$DataKeuangan->KeteranganText}}</td>
          <td class="nowrap" align="right">
            @if ($DataKeuangan->tipe == 1)
              Rp. {{number_format($DataKeuangan->jumlah)}}
            @endif
          </td>
          <td class="nowrap" align="right">
            @if ($DataKeuangan->tipe == 2)
              Rp. {{number_format($DataKeuangan->jumlah)}}
            @endif
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
