<div class="content">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Tanggal</th>
        <th>Keterangan</th>
        <th>Jumlah</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($keuangan as $DataKeuangan)
        <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{HTanggal::FormatDate($DataKeuangan->created_at)}}</td>
          <td>{{$DataKeuangan->KeteranganText}}</td>
          <td>
            Rp. {{number_format($DataKeuangan->jumlah)}}
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
