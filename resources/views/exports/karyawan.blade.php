<div class="content">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Nama</th>
        <th>Tempat,Tanggal Lahir</th>
        <th>Alamat</th>
        <th>No Handphone</th>
        <th>Jabatan</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($Karyawan as $Index=>$DataKaryawan)
        <tr>
          <td>{{$Index+1}}</td>
          <td>{{$DataKaryawan->nama}}</td>
          <td>{{$DataKaryawan->TTL}}</td>
          <td>{!!nl2br($DataKaryawan->alamat)!!}</td>
          <td>{{$DataKaryawan->no_handphone}}</td>
          <td>{{$DataKaryawan->Jabatan->nama}}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
