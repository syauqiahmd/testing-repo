<div class="content">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>#</th>
        <th>Nama</th>
        <th>Tempat, Tanggal Lahir</th>
        <th>Alamat</th>
        <th>No Handphone</th>
        <th>Jumlah Kursus</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($Peserta as $Index=>$DataPeserta)
        <tr>
          <td>{{$Index+1}}</td>
          <td>{{$DataPeserta->nama}}</td>
          <td>{{$DataPeserta->TTL}}</td>
          <td>{!!nl2br($DataPeserta->alamat)!!}</td>
          <td>{{$DataPeserta->no_handphone}}</td>
          <td class="text-center">{{$DataPeserta->Pendaftaran->count()}}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
