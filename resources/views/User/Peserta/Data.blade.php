@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data User Peserta</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('userPesertaTambahForm')}}" type="button" name="button" class="btn btn-info">Tambah</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Tempat,Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>No Handphone</th>
                    <th>Username</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Peserta as $Index=>$DataPeserta)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{$DataPeserta->nama}}</td>
                      <td>{{$DataPeserta->TTL}}</td>
                      <td>{!!nl2br($DataPeserta->alamat)!!}</td>
                      <td>{{$DataPeserta->no_handphone}}</td>
                      <td>{{$DataPeserta->User->username}}</td>
                      <td>
                        <a href="{{Route('userPesertaEditForm', ['id' => $DataPeserta->UUID])}}" class="btn btn-sm btn-round btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a>
                        <button data={{$DataPeserta->UUID}} href={{Route('userPesertaHapus')}} class="btn btn-sm btn-round btn-danger btn-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
