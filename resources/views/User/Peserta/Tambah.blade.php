@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Tambah Peserta</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('userPesertaData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form action="{{Route('userPesertaTambahSubmit')}}" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Nama</label>
                    <div class="col-md-12 col-xs-12">
                      <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{old('nama')}}" required>
                    </div>
                  </div>
                  <div class="form-group col-md-6 col-xs-12">
                    <label class="control-label col-xs-12">Tempat Lahir</label>
                    <div class="col-xs-12">
                      <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" value="{{old('tempat_lahir')}}" required>
                    </div>
                  </div>
                  <div class="form-group col-md-6 col-xs-12">
                    <label class="control-label col-xs-12">Tanggal Lahir</label>
                    <div class="col-xs-12">
                      <input type="date" class="form-control" placeholder="Tanggal Lahir" name="tanggal_lahir" value="{{old('tanggal_lahir')}}" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Alamat</label>
                    <div class="col-xs-12">
                      <textarea class="form-control" name="alamat" rows="2" name="alamat" required>{{old('alamat')}}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">No. Handphone</label>
                    <div class="col-md-12 col-xs-12">
                      <input type="text" class="form-control" placeholder="No. Handphone" name="no_handphone" value="{{old('no_handphone')}}" required>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Username</label>
                    <div class="col-md-12 col-xs-12">
                      <input type="text" class="form-control" placeholder="Username" name="username" value="{{old('username')}}" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Password</label>
                    <div class="col-md-12 col-xs-12">
                      <input type="password" class="form-control" placeholder="Password" name="password" required>
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-info">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
