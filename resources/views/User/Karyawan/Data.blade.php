@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data User Karyawan</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('userKaryawanTambahForm')}}" type="button" name="button" class="btn btn-info">Tambah</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Tempat,Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>No Handphone</th>
                    <th>Jabatan</th>
                    <th>Username</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Karyawan as $Index=>$DataKaryawan)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{$DataKaryawan->nama}}</td>
                      <td>{{$DataKaryawan->TTL}}</td>
                      <td>{!!nl2br($DataKaryawan->alamat)!!}</td>
                      <td>{{$DataKaryawan->no_handphone}}</td>
                      <td>{{$DataKaryawan->Jabatan->nama}}</td>
                      <td>{{$DataKaryawan->User->username}}</td>
                      <td>
                        <a href="{{Route('userKaryawanEditForm', ['id' => $DataKaryawan->UUID])}}" class="btn btn-sm btn-round btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a>
                        <button data={{$DataKaryawan->UUID}} href={{Route('userKaryawanHapus')}} class="btn btn-sm btn-round btn-danger btn-delete" data-toggle="tooltip" data-placement="bottom" title="Hapus" {!!$DataKaryawan->id == Auth::User()->Data->id ? 'status="Tidak Dapat Menghapus Data Sedang Login"' : ''!!}><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
