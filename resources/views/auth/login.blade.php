@extends('Layouts.App')
@section('section')
  <body class="login">
    <div>
      <div class="login_wrapper">
        <section class="login_content">
          <img class="img-fluid mb-5 d-block mx-auto" src="{!!asset('img/logo/logobpc.png')!!}" alt="">
          {{-- <h2>Lembaga Kursus Bina Pelaihari Computer</h2> --}}
          <form action="{{route('Login')}}" method="POST">
            @csrf
            <h1>Login Form</h1>
            <div>
              <input type="text" class="form-control" name="username" placeholder="Username" required>
            </div>
            <div>
              <input type="password" class="form-control" name="password" placeholder="Password" required>
            </div>
            <div>
              <button class="btn btn-secondary submit" href="index.html">Log in</button>
              <a class="reset_pass" href="{!!route('Dashboard')!!}">Kembali ke Landing Page</a>
            </div>
          </form>
        </section>
      </div>
    </div>
  </body>
@endsection
