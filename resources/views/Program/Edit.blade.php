@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Edit Program</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('programData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form action="{{Route('programEditSubmit', ['id' => $Program->UUID])}}" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Nama Program</label>
                    <div class="col-xs-12">
                      <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{$Program->nama}}" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Jumlah Pertemuan</label>
                    <div class="col-xs-12">
                      <input type="number" class="form-control" placeholder="Jumlah Pertemuan" name="jumlah_pertemuan" min="1" value="{{$Program->jumlah_pertemuan}}" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Biaya</label>
                    <div class="col-xs-12">
                      <input type="number" class="form-control" placeholder="Biaya" name="biaya" min="0" step="1000" value="{{$Program->biaya}}" required>
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-info">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
