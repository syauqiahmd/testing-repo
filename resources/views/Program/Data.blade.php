@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Program</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('programTambahForm')}}" type="button" name="button" class="btn btn-info">Tambah</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Program</th>
                    <th>Pertemuan</th>
                    <th>Biaya</th>
                    <th class="text-center">Data</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Program as $Index=>$DataProgram)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{$DataProgram->nama}}</td>
                      <td>{{$DataProgram->jumlah_pertemuan}}</td>
                      <td align="right">Rp. {{number_format($DataProgram->biaya)}}</td>
                      <td align="center">
                        <a href="{{Route('programJadwalData', ['idProgram' => $DataProgram->UUID])}}" class="btn btn-sm btn-round btn-success" data-toggle="tooltip" data-placement="bottom" title="Lihat Jadwal"><i class="fa fa-calendar-o"></i></a>
                        <a href="{{Route('programMateriData', ['idProgram' => $DataProgram->UUID])}}" class="btn btn-sm btn-round btn-info" data-toggle="tooltip" data-placement="bottom" title="Lihat Materi Program"><i class="fa fa-book"></i></a>
                      </td>
                      <td>
                        <a href="{{Route('programEditForm', ['id' => $DataProgram->UUID])}}" class="btn btn-sm btn-round btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a>
                        <button data={{$DataProgram->UUID}} href={{Route('programHapus')}} class="btn btn-sm btn-round btn-danger btn-delete" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
