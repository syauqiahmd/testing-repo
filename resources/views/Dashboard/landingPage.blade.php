<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Bina Pelaihari Computer</title>

  <!-- Bootstrap core CSS -->
  <link href="{{asset('css/landingpage/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{asset('css/landingpage/fontawesome.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- Plugin CSS -->
  <link href="{{asset('css/landingpage/magnific-popup.css')}}" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template -->
  <link href="{{asset('css/landingpage/app.css')}}" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">BPC</a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button"
      data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
      aria-label="Toggle navigation">
      Menu
      <i class="fas fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item mx-0 mx-lg-1">
          <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">Tentang Kami</a>
        </li>
        <li class="nav-item mx-0 mx-lg-1">
          <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Materi</a>
        </li>
        <li class="nav-item mx-0 mx-lg-1">
          <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#syarat">Syarat Peserta</a>
        </li>
        <li class="nav-item mx-0 mx-lg-1">
          <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Kontak</a>
        </li>
        <li class="nav-item mx-0 mx-lg-1">
          <a class="nav-link py-3 px-0 px-lg-3 rounded" href="{!!route('LoginForm')!!}">Login</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<!-- Header -->
<header class="masthead bg-primary text-white text-center">
  <div class="container">
    <img class="img-fluid mb-5 d-block mx-auto" src="{!!asset('img/logo/logobpc.png')!!}" alt="">
    <h1 class="text-uppercase mb-0">{{$pengaturan->nama??''}}</h1>
    <hr class="star-light">
    <h2 class="font-weight-light mb-0">Lembaga Pelatihan Komputer</h2>
  </div>
</header>

<!-- About Section -->
<section class="mb-0" id="about">
  <div class="container">
    <h2 class="text-center text-secondary text-uppercase">Tentang Kami</h2>
    <hr class="star-dark mb-5">
    <div class="row">
      <div class="col-lg-12 mr-auto text-center">
        <p class="lead">{!!nl2br($pengaturan->tentang??'')!!}</p>
      </div>
    </div>
  </div>
</section>
<section class="portfolio" id="portfolio">
  <div class="container">
    <h2 class="text-center text-uppercase text-secondary mb-0">Materi</h2>
    <hr class="star-dark mb-5">
    <table class="table">
      <thead>
        <th>#</th>
        <th>Nama Materi</th>
        <th>Jumlah Pertemuan</th>
        <th>Biaya</th>
      </thead>
      <tbody>
        @foreach ($program as $dataProgram)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$dataProgram->nama}}</td>
            <td>{{$dataProgram->jumlah_pertemuan}}</td>
            <td>Rp. {{number_format($dataProgram->biaya)}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</section>
<section class="syarat" id="syarat">
  <div class="container">
    <h2 class="text-center text-uppercase text-secondary mb-0">Syarat Peserta</h2>
    <hr class="star-dark mb-5">
    <div class="row">
      <div class="col-lg-12 mr-auto text-center">
        <p class="lead">{!!nl2br($pengaturan->syarat??'')!!}</p>
      </div>
    </div>
  </div>
</section>
<footer id="contact" class="footer text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-6 mb-5 mb-lg-0">
        <h4 class="text-uppercase mb-4">Informasi Pendaftaran</h4>
        <p class="lead mb-0">{!!nl2br($pengaturan->kontak??'')!!}</p>
      </div>

      <div class="col-md-6">
        <h4 class="text-uppercase mb-4">Alamat Lengkap</h4>
        <p class="lead mb-0">{{$pengaturan->alamat??''}}</p>
      </div>
    </div>
  </div>
</footer>

<div class="copyright py-4 text-center text-white">
  <div class="container">
    <small>Copyright &copy; Bina Pelaihari Komputer 2018</small>
  </div>
</div>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-to-top d-lg-none position-fixed ">
  <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
    <i class="fa fa-chevron-up"></i>
  </a>
</div>

<!-- Bootstrap core JavaScript -->
<script src="{{asset('js/landingpage/jquery.js')}}"></script>
<script src="{{asset('js/landingpage/bootstrap.bundle.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="{{asset('js/landingpage/jquery.easing.js')}}"></script>
<script src="{{asset('js/landingpage/jquery.magnific-popup.js')}}"></script>

<!-- Contact Form JavaScript -->
{{-- <script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script> --}}

<!-- Custom scripts for this template -->
<script src="{{asset('js/landingpage/freelancer.js')}}"></script>

</body>

</html>
