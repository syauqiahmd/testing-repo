@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Dashboard</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row top_tiles">
        <div class="animated flipInY col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-male"></i></div>
            <div class="count">{{$Karyawan}}</div>
            <h3>Karyawan</h3>
          </div>
        </div>
        <div class="animated flipInY col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-user"></i></div>
            <div class="count">{{$Peserta}}</div>
            <h3>Peserta</h3>
          </div>
        </div>
      </div>
      <h3>Statistik Program</h3>
      <div class="row top_tiles">
        @foreach ($Program as $dataProgram)
          <div class="animated flipInY col-lg-4 col-sm-6 col-xs-12">
            <div class="tile-stats">
              <div class="icon"><i class="fa fa-book"></i></div>
              <div class="count">{{$dataProgram->Pendaftaran->count()}}</div>
              <h3>{{$dataProgram->nama}}</h3>
            </div>
          </div>
        @endforeach
      </div>
      <h3>Statistik Keuangan</h3>
      <div class="row top_tiles">
        <div class="animated flipInY col-lg-4 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-caret-square-o-up"></i></div>
            <div class="count text-2em">Rp. {{number_format($Keuangan->whereTipe(1)->sum('jumlah'))}}</div>
            <h3>Masuk</h3>
          </div>
        </div>
        <div class="animated flipInY col-lg-4 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-caret-square-o-down"></i></div>
            <div class="count text-2em">Rp. {{number_format($Keuangan->whereTipe(2)->sum('jumlah'))}}</div>
            <h3>Keluar</h3>
          </div>
        </div>
        <div class="animated flipInY col-lg-4 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-square-o"></i></div>
            <div class="count text-2em">Rp. {{number_format($Keuangan->whereTipe(1)->sum('jumlah')-$Keuangan->whereTipe(2)->sum('jumlah'))}}</div>
            <h3>Total</h3>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <h1 class="text-center">Halo, {{Auth::User()->Data->nama}}</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
