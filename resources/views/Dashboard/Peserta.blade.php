@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Dashboard Peserta</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
              @foreach ($Pendaftaran as $Index=>$DataPendaftaran)
                <div class="panel">
                  <a class="panel-heading collapsed" role="tab" id="heading{{$Index+1}}" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$Index+1}}" aria-expanded="false" aria-controls="collapse{{$Index+1}}">
                    <h4 class="panel-title">Jadwal Selanjutnya Pendaftaran #{{$DataPendaftaran->id}}</h4>
                  </a>
                  <div id="collapse{{$Index+1}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$Index+1}}" aria-expanded="false" style="">
                    <div class="panel-body">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Nama Program</th>
                            <th>Jadwal Program</th>
                            <th>Jadwal Masuk</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($DataPendaftaran->AllJadwal->sortBy('Jadwal') as $Index=>$DataJadwal)
                            <tr>
                              <td>{{$loop->iteration}}</td>
                              <td>{{$DataJadwal['Data']->Program->nama}}</td>
                              <td>{{HTanggal::FormatHariID($DataJadwal['Data']->hari)}} | {{$DataJadwal['Data']->Jam}}</td>
                              <td>{{HTanggal::FormatDate($DataJadwal['Jadwal'])}} | {{$DataJadwal['Data']->Jam}}</td>
                              <td>{!!HTanggal::PassDate($DataJadwal['Jadwal'])? '<i class="fa fa-check-circle fa-2x text-success"></i>':'<i class="fa fa-minus-circle fa-2x"></i>'!!}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
