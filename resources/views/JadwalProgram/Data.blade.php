@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Jadwal Program <small>{{$Program->nama}}</small></h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('programData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <a href="{{Route('programJadwalTambahForm', ['idProgram' => $Program->UUID])}}" type="button" name="button" class="btn btn-info">Tambah</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Hari</th>
                    <th>Jam</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Jadwal as $Index=>$DataJadwal)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{HTanggal::FormatHariID($DataJadwal->hari)}}</td>
                      <td>{{$DataJadwal->Jam}}</td>
                      <td>
                        <a href="{{Route('programJadwalEditForm', ['idProgram' => $Program->UUID,'id' => $DataJadwal->UUID])}}" class="btn btn-sm btn-round btn-primary" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i></a>
                        <button data={{$DataJadwal->UUID}} href={{Route('programJadwalHapus', ['idProgram' => $Program->UUID])}} class="btn btn-sm btn-round btn-danger btn-delete" data-toggle="tooltip" data-placement="bottom" title="Delete"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
