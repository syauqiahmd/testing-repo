@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Tambah Jadwal Program <small>{{$Program->nama}}</small></h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('programJadwalData', ['idProgram' => $Program->UUID])}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form class="form-horizontal form-label-left" action="{{Route('programJadwalEditSubmit', ['idProgram' => $Program->UUID, 'id' => $Jadwal->UUID])}}" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Hari</label>
                    <div class="col-md-10 col-xs-12">
                      <select class="form-control" name="hari" required>
                        <option value="" hidden selected>Hari</option>
                        @foreach (HTanggal::FormatHariID() as $Index=>$Hari)
                          <option value="{{$Index+1}}" {{$Index+1==$Jadwal->hari? 'selected' : ''}}>{{$Hari}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Waktu Mulai</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="time" class="form-control" name="jam_awal" value="{{$Jadwal->jam_awal}}" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Waktu Selesai</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="time" class="form-control" name="jam_akhir" value="{{$Jadwal->jam_akhir}}" required>
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-info">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
