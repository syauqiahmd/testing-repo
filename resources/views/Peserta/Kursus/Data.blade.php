@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Program</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Peserta</th>
                    <th>Jumlah Program</th>
                    <th>Tanggal Daftar</th>
                    <th>Status</th>
                    <th>Status Pembayaran</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Pendaftaran as $Index=>$DataPendaftaran)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{$DataPendaftaran->Peserta->nama}}</td>
                      <td align="center">{{$DataPendaftaran->Program->count()}}</td>
                      <td>{{HTanggal::FormatDate($DataPendaftaran->tanggal_daftar)}}</td>
                      <td align="center">{{$DataPendaftaran->StatusKursus}}</td>
                      <td align="center">{{$DataPendaftaran->StatusBayar}}</td>
                      <td>
                        <a href="{{Route('kursusInfo', ['id' => $DataPendaftaran->UUID])}}" class="btn btn-sm btn-round btn-success" data-toggle="tooltip" data-placement="bottom" title="Info"><i class="fa fa-info-circle"></i></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
