@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Info Pendaftaran #{{$Pendaftaran->id}}</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('kursusData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="row">
                <dl class="col-sm-12">
                  <legend>Data Peserta</legend>
                  <dl>
                    <dt class="col-sm-4">Nama</dt>
                    <dd class="col-sm-8">{{$Pendaftaran->Peserta->nama}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Tempat, Tanggal Lahir</dt>
                    <dd class="col-sm-8">{{$Pendaftaran->Peserta->TTL}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Alamat</dt>
                    <dd class="col-sm-8">{!!nl2br($Pendaftaran->Peserta->alamat)!!}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">No Handphone</dt>
                    <dd class="col-sm-8">{{$Pendaftaran->Peserta->no_handphone}}</dd>
                  </dl>
                </dl>
                <dl class="col-sm-12">
                  <legend>Data Program</legend>
                  <dl>
                    <dt class="col-sm-1">#</dt>
                    <dt class="col-sm-3">Program</dt>
                    <dt class="col-sm-2">Nilai</dt>
                    <dl class="col-sm-6 dl-program">
                      <dt>Jadwal Dipilih</dt>
                    </dl>
                    <hr>
                    @foreach ($Pendaftaran->PendaftaranProgram as $DataProgram)
                      <dl>
                        <dt class="col-sm-1">{{$loop->iteration}}</dt>
                        <dt class="col-sm-3">{{$DataProgram->Program->nama}}</dt>
                        <dt class="col-sm-2">{{round($DataProgram->Nilai->avg('nilai'),2)?:"Belum Ada Nilai"}}</dt>
                        <dl class="col-sm-6 dl-program">
                          @foreach ($DataProgram->JadwalProgram->sortBy('hari') as $DataJadwal)
                            <dd>{{HTanggal::FormatHariID($DataJadwal->hari)}} [{{"{$DataJadwal->jam_awal} - {$DataJadwal->jam_akhir}"}}]</dd>
                          @endforeach
                        </dl>
                      </dl>
                      <dl class="dl-legend">
                        <legend>Materi <strong>{{$DataProgram->Program->nama}}</strong></legend>
                      </dl>
                      @if (!$DataProgram->Program->Materi->count())
                        <h4 class="text-center">Data Materi Belum Ada</h4>
                      @endif
                      @foreach ($DataProgram->Program->Materi as $DataMateri)
                        <dl>
                          <dt class="col-sm-1 text-right">{{$loop->iteration}}</dt>
                          <dt class="col-sm-6">{{$DataMateri->nama}}</dt>
                          <dt class="col-sm-5">{{$DataProgram->Nilai->where('materi_id', $DataMateri->id)->first()->nilai??"Belum Ada Nilai"}}</dt>
                        </dl>
                      @endforeach
                      <hr>
                    @endforeach
                  </dl>
                </dl>
                <dl class="col-sm-12">
                  <legend>Data Keuangan</legend>
                  <dl>
                    <dt class="col-sm-4">Total Biaya</dt>
                    <dd class="col-sm-8">Rp. {{number_format($Pendaftaran->total_bayar)}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Total Dibayar</dt>
                    <dd class="col-sm-8">Rp. {{number_format($Pendaftaran->TotalDibayar)}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Sisa</dt>
                    <dd class="col-sm-8">Rp. {{number_format($Pendaftaran->SisaBayar)}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Status</dt>
                    <dd class="col-sm-8"><b>{{$Pendaftaran->StatusBayar}}</b></dd>
                  </dl>
                </dl>
                <dl class="col-sm-12">
                  <legend>Riwayat Pembayaran</legend>
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Jumlah Bayar</th>
                        <th>Tanggal Bayar</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($Pendaftaran->Keuangan->sortByDesc('created_at') as $DataKeuangan)
                        <tr>
                          <td>{{$loop->iteration}}</td>
                          <td align=right>Rp. {{number_format($DataKeuangan->jumlah)}}</td>
                          <td>{{HTanggal::FormatDate($DataKeuangan->created_at)}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
