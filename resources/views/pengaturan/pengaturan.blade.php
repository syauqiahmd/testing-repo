@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Pengaturan Aplikasi</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <form class="form-horizontal form-label-left" action="{{Route('pengaturanSubmit')}}" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Nama Lembaga</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{$pengaturan->nama??null}}" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Alamat Lembaga</label>
                    <div class="col-md-10 col-xs-12">
                      <textarea class="form-control" placeholder="Alamat" name="alamat" required>{{$pengaturan->alamat??null}}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Deskripsi Lembaga</label>
                    <div class="col-md-10 col-xs-12">
                      <textarea class="form-control" placeholder="Tentang" name="tentang" required>{{$pengaturan->tentang??null}}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Kontak</label>
                    <div class="col-md-10 col-xs-12">
                      <textarea class="form-control" placeholder="Kontak" name="kontak" required>{{$pengaturan->kontak??null}}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Syarat</label>
                    <div class="col-md-10 col-xs-12">
                      <textarea class="form-control" placeholder="Syarat" name="syarat" required>{{$pengaturan->syarat??null}}</textarea>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Ketua</label>
                    <div class="col-md-10 col-xs-12">
                      <select class="form-control" name="karyawan_id">
                        <option value="" hidden>Pilih</option>
                        @foreach ($karyawan as $dataKaryawan)
                          <option value="{{$dataKaryawan->id}}" @if (isset($pengaturan->karyawan_id) && ($pengaturan->karyawan_id == $dataKaryawan->id)) selected @endif>{{$dataKaryawan->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-info">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
