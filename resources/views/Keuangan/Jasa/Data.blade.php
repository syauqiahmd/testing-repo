@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Keuangan Jasa</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('keuanganJasaTambahForm')}}" type="button" name="button" class="btn btn-info">Tambah</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Tanggal</th>
                    <th>Keterangan</th>
                    <th>Jumlah</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Keuangan as $Index=>$DataKeuangan)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{HTanggal::FormatDate($DataKeuangan->created_at)}}</td>
                      <td>{{$DataKeuangan->keterangan}}</td>
                      <td>Rp. {{number_format($DataKeuangan->jumlah)}}</td>
                      <td>
                        <a href="{{Route('keuanganJasaEditForm', ['id' => $DataKeuangan->UUID])}}" class="btn btn-sm btn-round btn-primary"><i class="fa fa-pencil"></i></a>
                        <button data={{$DataKeuangan->UUID}} href={{Route('keuanganJasaHapus')}} class="btn btn-sm btn-round btn-danger btn-delete"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
