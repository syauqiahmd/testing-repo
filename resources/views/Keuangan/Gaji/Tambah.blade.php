@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Tambah Keuangan Gaji</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('keuanganGajiData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form class="form-horizontal form-label-left" action="{{Route('keuanganGajiTambahSubmit')}}" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Nama Karyawan</label>
                    <div class="col-md-10 col-xs-12">
                      <select id="karyawanIdGaji" class="form-control select2" name="karyawan_id" required>
                        <option value="" selected hidden>Nama Karyawan</option>
                        @foreach ($Karyawan as $DataKaryawan)
                          <option value="{{$DataKaryawan->id}}" data-gaji="{{number_format($DataKaryawan->gaji)}}">{{$DataKaryawan->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Jumlah Gaji</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" value="Rp. 0" id="gajiKaryawan" disabled>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Periode Gaji</label>
                    <div class="col-md-10 col-xs-12">
                      <select id="periodeGaji" class="form-control select2" name="periode_gaji" required>
                        @foreach (HTanggal::Periode() as $Date)
                          <option value="{{$Date['date']}}" {{HTanggal::EqualDate(Carbon\Carbon::now()->firstOfMonth(), $Date['date'])? 'selected':''}}>{{$Date['text']}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Status Gaji</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" placeholder="Status Gaji Bulan Ini" id="statusGaji" disabled>
                    </div>
                  </div>
                  <div class="text-center">
                    <button id="submitGaji" type="submit" class="btn btn-info">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
