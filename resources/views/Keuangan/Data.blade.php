@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Keuangan</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <div class="row top_tiles">
                <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-toggle-down"></i></div>
                    <div class="count text-2em">Rp. {{number_format($Keuangan->where('tipe', 1)->sum('jumlah'))}}</div>
                    <h3>Masuk</h3>
                  </div>
                </div>
                <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-toggle-up"></i></div>
                    <div class="count text-2em">Rp. {{number_format($Keuangan->where('tipe', 2)->sum('jumlah'))}}</div>
                    <h3>Keluar</h3>
                  </div>
                </div>
                <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-square-o"></i></div>
                    <div class="count text-2em">
                      Rp. {{number_format($Keuangan->where('tipe', 1)->sum('jumlah') - $Keuangan->where('tipe', 2)->sum('jumlah'))}}
                    </div>
                    <h3>Total</h3>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Tanggal</th>
                    <th>Tipe</th>
                    <th>Sumber</th>
                    <th>Keterangan</th>
                    <th>Jumlah Masuk</th>
                    <th>Jumlah Keluar</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Keuangan as $DataKeuangan)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{HTanggal::FormatDate($DataKeuangan->created_at)}}</td>
                      <td>{{$DataKeuangan->TipeText}}</td>
                      <td>{{$DataKeuangan->SumberText}}</td>
                      <td>{{$DataKeuangan->KeteranganText}}</td>
                      <td>
                        @if ($DataKeuangan->tipe == 1)
                          Rp. {{number_format($DataKeuangan->jumlah)}}
                        @endif
                      </td>
                      <td>
                        @if ($DataKeuangan->tipe == 2)
                          Rp. {{number_format($DataKeuangan->jumlah)}}
                        @endif
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
