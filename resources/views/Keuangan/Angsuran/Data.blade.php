@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Angsuran</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Peserta</th>
                    <th>Tanggal Daftar</th>
                    <th>Total Biaya</th>
                    <th>Jumlah Dibayar</th>
                    <th>Sisa Bayar</th>
                    <th>Status Kursus</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Pendaftaran as $Index=>$DataPendaftaran)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{$DataPendaftaran->Peserta->nama}}</td>
                      <td>{{HTanggal::FormatDate($DataPendaftaran->tanggal_daftar)}}</td>
                      <td align="right">Rp. {{number_format($DataPendaftaran->total_bayar)}}</td>
                      <td align="right">Rp. {{number_format($DataPendaftaran->TotalDibayar)}}</td>
                      <td align="right">
                        @if ($DataPendaftaran->StatusBayar == 'Lunas')
                          <div class="alert alert-success alert-sm">
                            <strong>Lunas</strong>
                          </div>
                        @else
                          Rp. {{number_format($DataPendaftaran->SisaBayar)}}
                        @endif
                      </td>
                      <td>{{$DataPendaftaran->StatusKursus}}</td>
                      <td>
                        <a href="{{Route('keuanganAngsuranInfo', ['id' => $DataPendaftaran->UUID])}}" class="btn btn-sm btn-round btn-success" data-toggle="tooltip" data-placement="bottom" title="Info"><i class="fa fa-info-circle"></i></a>
                        @if ($DataPendaftaran->StatusBayar != 'Lunas')
                          <a href="{{Route('keuanganAngsuranBayar', ['id' => $DataPendaftaran->UUID])}}" class="btn btn-sm btn-round btn-primary" data-toggle="tooltip" data-placement="bottom" title="Bayar"><i class="fa fa-money"></i></a>
                        @endif
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
