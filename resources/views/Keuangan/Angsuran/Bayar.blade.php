@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Tambah Bayar Angsuran #{{$Pendaftaran->id}}</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('pendaftaranData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form action="{{Route('keuanganAngsuranBayarSubmit', ['id' => $Pendaftaran->UUID])}}" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Total Biaya Kursus</label>
                    <div id="total_biaya" class="col-xs-12">
                      <input type="text" class="form-control biaya-show" value="Rp. {{number_format($Pendaftaran->total_bayar)}}" disabled>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Total Sisa Biaya</label>
                    <div id="total_biaya" class="col-xs-12">
                      <input type="text" class="form-control biaya-show" value="Rp. {{number_format($Pendaftaran->SisaBayar)}}" disabled>
                      <input type="hidden" class="form-control biaya-hidden" name="total_bayar" value="{{$Pendaftaran->SisaBayar}}" disable>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Total Bayar</label>
                    <div class="col-xs-12">
                      <input id="total_bayar" type="number" name="jumlah" min=0 value="0" max={{$Pendaftaran->SisaBayar}} class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Sisa Bayar</label>
                    <div class="col-xs-12">
                      <input id="sisa_bayar" type="text" class="form-control" value="Rp. 0" disabled>
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-info">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
