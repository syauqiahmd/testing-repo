@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Tambah Keuangan Operasional</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('keuanganOperasionalData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form class="form-horizontal form-label-left" action="{{Route('keuanganOperasionalEditSubmit', ['id' => $Keuangan->UUID])}}" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Jumlah</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="number" class="form-control" placeholder="Jumlah Biaya Operasional" name="jumlah" min=0 value="{{$Keuangan->jumlah}}" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Keterangan</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" placeholder="Keterangan" name="keterangan" value="{{$Keuangan->keterangan}}" required>
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-info">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
