@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Pilih Jadwal Program</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <form action="{{Route('pendaftaranJadwalSubmit', ['id' => $Pendaftaran->UUID])}}" method="post">
                @csrf
                <div class="col-md-12 col-xs-12">
                  <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach ($PendaftaranProgram as $Index=>$DataProgram)
                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="heading{{$Index+1}}" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$Index+1}}" aria-expanded="false" aria-controls="collapse{{$Index+1}}">
                          <h4 class="panel-title">Jadwal Program {{$DataProgram->Program->nama}}</h4>
                        </a>
                        <div id="collapse{{$Index+1}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$Index+1}}" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                            <p><strong>Pilih Pengajar</strong></p>
                              <div class="col-md-12 col-xs-12 no-padding">
                                <select class="form-control" name="karyawan_id[{{$DataProgram->id}}]">
                                  <option value="">Pilih Pengajar</option>
                                  @foreach ($Karyawan as $dataKaryawan)
                                    <option value="{{$dataKaryawan->id}}">{{$dataKaryawan->nama}}</option>
                                  @endforeach
                                </select>
                              </div>
                            <p><strong>Pilih Jadwal</strong></p>
                            <div class="table-form">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Hari</th>
                                    <th>Jam</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($DataProgram->Program->JadwalProgram as $Index=>$DataJadwal)
                                    <tr>
                                      <td>
                                        <input type="checkbox" class="jadwal-checkbox" value="{{$DataJadwal->id}}" name="jadwal_program_id[{{$DataProgram->id}}][]" data-program="{{"{$DataProgram->Program->id}"}}" required>
                                      </td>
                                      <td>{{HTanggal::FormatHariID($DataJadwal->hari)}}</td>
                                      <td>{{$DataJadwal->Jam}}</td>
                                    </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach
                  </div>
                  <div class="text-center">
                    <button id="jadwalSubmit" type="submit" class="btn btn-info">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
