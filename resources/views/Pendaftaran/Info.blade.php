@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Info Pendaftaran #{{$Pendaftaran->id}}</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('pendaftaranData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="row">
                <dl class="col-sm-12">
                  <legend>Data Peserta</legend>
                  <dl>
                    <dt class="col-sm-4">Nama</dt>
                    <dd class="col-sm-8">{{$Pendaftaran->Peserta->nama}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Tempat, Tanggal Lahir</dt>
                    <dd class="col-sm-8">{{$Pendaftaran->Peserta->TTL}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Alamat</dt>
                    <dd class="col-sm-8">{!!nl2br($Pendaftaran->Peserta->alamat)!!}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">No Handphone</dt>
                    <dd class="col-sm-8">{{$Pendaftaran->Peserta->no_handphone}}</dd>
                  </dl>
                </dl>
                <dl class="col-sm-12">
                  <legend>Data Program</legend>
                  <dl>
                    <dt class="col-sm-1">#</dt>
                    <dt class="col-sm-3">Program</dt>
                    <dl class="col-sm-8 dl-program">
                      <dt>Jadwal Dipilih</dt>
                    </dl>
                    <hr>
                  @foreach ($Pendaftaran->PendaftaranProgram as $Index=>$DataProgram)
                    <dl>
                      <dt class="col-sm-1">{{$Index+1}}</dt>
                      <dt class="col-sm-3">{{$DataProgram->Program->nama}}</dt>
                      <dl class="col-sm-8 dl-program">
                        @foreach ($DataProgram->JadwalProgram as $DataJadwal)
                          <dd>{{HTanggal::FormatHariID($DataJadwal->hari)}} [{{"{$DataJadwal->jam_awal} - {$DataJadwal->jam_akhir}"}}]</dd>
                        @endforeach
                      </dl>
                      <hr>
                    </dl>
                  @endforeach
                </dl>
                <dl class="col-sm-12">
                  <legend>Data Keuangan</legend>
                  <dl>
                    <dt class="col-sm-4">Total Biaya</dt>
                    <dd class="col-sm-8">Rp. {{number_format($Pendaftaran->total_bayar)}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Total Dibayar</dt>
                    <dd class="col-sm-8">Rp. {{number_format($Pendaftaran->TotalDibayar)}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Sisa</dt>
                    <dd class="col-sm-8">Rp. {{number_format($Pendaftaran->SisaBayar)}}</dd>
                  </dl>
                  <dl>
                    <dt class="col-sm-4">Status</dt>
                    <dd class="col-sm-8">{{$Pendaftaran->StatusBayar}}</dd>
                  </dl>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
