@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Pendaftaran Peserta Kursus</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('pendaftaranData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form action="{{Route('pendaftaranTambahSubmit')}}" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Tanggal Daftar</label>
                    <div class="col-xs-12">
                      <input type="date" class="form-control" value="{{HTanggal::Now()}}" name="tanggal_daftar" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Nama Peserta</label>
                    <div class="col-xs-12">
                      <select class="form-control select2" name="peserta_id" required>
                        <option value="" selected hidden>Peserta</option>
                        @foreach ($Peserta as $DataPeserta)
                          <option value="{{$DataPeserta->id}}" {{old('peserta_id') == $DataPeserta->id? 'selected':''}}>{{"{$DataPeserta->nama} ({$DataPeserta->no_handphone})"}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Pilih Program</label>
                    <div class="col-xs-12">
                      <div class="table-form">
                        <table class="table datatable-form">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Nama Program</th>
                              <th>Jumlah Pertemuan</th>
                              <th>Biaya</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($Program as $Index=>$DataProgram)
                              <tr>
                                <td>
                                  <input type="checkbox" class="program-checkbox" data-biaya="{{$DataProgram->biaya}}" value="{{$DataProgram->id}}" name="program_id[]">
                                </td>
                                <td>{{$DataProgram->nama}}</td>
                                <td align="center">{{$DataProgram->jumlah_pertemuan}}</td>
                                <td align="right">Rp. {{number_format($DataProgram->biaya)}}</td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Total Biaya</label>
                    <div id="total_biaya" class="col-xs-12">
                      <input type="text" class="form-control biaya-show" value="Rp. 0" disabled required>
                      <input type="hidden" class="form-control biaya-hidden" name="total_bayar" value="0" disable required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Total Bayar</label>
                    <div class="col-xs-12">
                      <input id="total_bayar" name="bayar" min=0 value="0" type="number" class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-xs-12">Sisa Bayar</label>
                    <div class="col-xs-12">
                      <input id="sisa_bayar" type="text" class="form-control" value="Rp. 0" disabled>
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-info">Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
