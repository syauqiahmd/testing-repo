<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="{{Route('Dashboard')}}"><i class="fa fa-home"></i>Home</a></li>
      @if (Auth::User()->tipe == 2)
        <li><a href="{{Route('kursusData')}}"><i class="fa fa-bookmark"></i>Kursus</a></li>
      @endif
      @if (Auth::User()->tipe == 1)
        <li><a><i class="fa fa-paperclip"></i> Master <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{Route('jabatanData')}}">Jabatan</a></li>
            <li><a href="{{Route('programData')}}">Program</a></li>
            <li><a href="{{Route('userKaryawanData')}}">Karyawan</a></li>
            <li><a href="{{Route('userPesertaData')}}">Peserta</a></li>
          </ul>
        </li>
        <li><a><i class="fa fa-certificate"></i> Transaksi <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{Route('pendaftaranData')}}">Pendaftaran Peserta</a></li>
            <li><a href="{{Route('nilaiData')}}">Nilai Peserta</a></li>
            <li>
              <a>Keuangan<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li>
                  <a href="{{Route('keuanganData')}}">Data Keuangan</a>
                </li>
                <li>
                  <a href="{{Route('keuanganAngsuranData')}}">Angsuran Peserta</a>
                </li>
                <li>
                  <a href="{{Route('keuanganJasaData')}}">Jasa</a>
                </li>
                <li>
                  <a href="{{Route('keuanganOperasionalData')}}">Operasional</a>
                </li>
                <li>
                  <a href="{{Route('keuanganGajiData')}}">Gaji</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li><a><i class="fa fa-file-text-o"></i> Laporan <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li>
              <a href="{{Route('laporanPeserta')}}">Peserta Kursus</a>
            </li>
            <li>
              <a href="{{Route('laporanPendaftaran')}}">Pendaftaran Kursus</a>
            </li>
            <li>
              <a href="{{Route('laporanProgram')}}">Program Kursus</a>
            </li>
            <li>
              <a href="{{Route('laporanKaryawan')}}">Karyawan</a>
            </li>
            <li>
              <a href="{{Route('laporanKeuangan')}}">Keuangan</a>
            </li>
            @foreach (HDefault::sumberKeuangan() as $dataSumberKeuangan)
              <li>
                <a href="{{Route('laporanKeuanganSpecific', ['tipe' => $dataSumberKeuangan])}}">Keuangan {{$dataSumberKeuangan}}</a>
              </li>
            @endforeach
            <li>
              <a href="{{Route('laporanSertifikat')}}">Sertifikat Peserta</a>
            </li>
            <li>
              <a href="{{Route('laporanSertifikat', ['nilai'])}}">Nilai Peserta</a>
            </li>
          </ul>
        </li>
        <li><a href="{{Route('pengaturan')}}"><i class="fa fa-gears"></i>Pengaturan</a></li>
      @endif
    </ul>
  </div>
</div>
