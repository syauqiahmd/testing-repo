<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Lembaga Kursus BPC</title>
  <link href="{{asset('css/app.css')}}" rel="stylesheet">
</head>
@yield('section')
<script src="{{asset('js/app.js')}}"></script>
@if (session('alert'))
  <script>notif('{{session('tipe')}}', '{{session('judul')}}', '{{session('pesan')}}')</script>
@endif
@if ($errors->count())
  <script>notif('error', 'Ada Kesalahan', '{{$errors->first()}}')</script>
@endif
</html>
