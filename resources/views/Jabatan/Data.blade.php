@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Data Jabatan</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('jabatanTambahForm')}}" type="button" name="button" class="btn btn-info">Tambah</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Jabatan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Jabatan as $Index=>$DataJabatan)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{$DataJabatan->nama}}</td>
                      <td>
                        <a href="{{Route('jabatanEditForm', ['id' => $DataJabatan->UUID])}}" class="btn btn-sm btn-round btn-primary"><i class="fa fa-pencil"></i></a>
                        <button data={{$DataJabatan->UUID}} href={{Route('jabatanHapus')}} class="btn btn-sm btn-round btn-danger btn-delete"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
