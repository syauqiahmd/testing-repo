@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Edit Nilai {{$PendaftaranProgram->Program->nama}}</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('nilaiDetail', ['id' => $PendaftaranProgram->Pendaftaran->UUID])}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form class="form-horizontal form-label-left" action="{{Route('nilaiEditSubmit', ['id' => $PendaftaranProgram->UUID])}}" method="post">
                @csrf
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                  <legend>Data Pendaftaran</legend>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Nama Peserta</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" value="{{$PendaftaranProgram->Pendaftaran->Peserta->nama}}" disabled>
                    </div>
                  </div>
                  <div class="form-group col-xs-12">
                    <label class="control-label col-md-2 col-xs-12">Program</label>
                    <div class="col-md-10 col-xs-12">
                      <input type="text" class="form-control" value="{{$PendaftaranProgram->Program->nama}}" disabled>
                    </div>
                  </div>
                  <legend>Form Penilaian</legend>
                  @if (!$PendaftaranProgram->Program->Materi->count())
                    <h1 class="text-center">Isi Data Materi Program Terlebih Dahulu</h1>
                  @endif
                  @foreach ($PendaftaranProgram->Program->Materi as $DataMateri)
                    <div class="form-group col-xs-12">
                      <label class="control-label col-md-2 col-xs-12">{{$DataMateri->nama}}</label>
                      <div class="col-md-10 col-xs-12">
                        <input type="number" class="form-control" placeholder="Nilai 0-100" name="materi[{{$DataMateri->id}}]" min="0" max="100" value="{{$PendaftaranProgram->Nilai->where('materi_id', $DataMateri->id)->first()->nilai??null}}" required>
                        <small>Nilai 0-100</small>
                      </div>
                    </div>
                  @endforeach
                  <div class="text-center">
                    <button type="submit" class="btn btn-info" {{$PendaftaranProgram->Program->Materi->count()?:"disabled"}}>Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
