@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>
            Detail Nilai Peserta {{"{$Pendaftaran->Peserta->nama}"}} <br>
            Nomor Pendaftaran #{{$Pendaftaran->id}}
          </h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('nilaiData')}}" type="button" name="button" class="btn btn-primary">Kembali</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Program</th>
                    <th>Nilai</th>
                    <th>Jadwal Terakhir</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Pendaftaran->PendaftaranProgram as $DataPendaftaranProgram)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$DataPendaftaranProgram->Program->nama}}</td>
                      <td align="center">{{round($DataPendaftaranProgram->Nilai->avg('nilai'),2)?:"Belum Ada Nilai"}}</td>
                      <td align="center">{{HTanggal::FormatDate($Pendaftaran->AllJadwal->where('Data.program_id', $DataPendaftaranProgram->program_id)->max('Jadwal'))}}</td>
                      <td>
                        <a href="{{Route('nilaiEdit', ['id' => $DataPendaftaranProgram->UUID])}}" class="btn btn-sm btn-round btn-primary" data-toggle="tooltip" data-placement="bottom" title="Ubah Nilai"><i class="fa fa-pencil"></i></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
