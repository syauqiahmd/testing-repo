@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Laporan Keuangan</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <div class="col-lg-4">
                <form action="{!!route('cetakKeuangan')!!}" method="post" class="float-left" target="_blank">
                  @csrf
                  <input type="hidden" name="keuangan" value="{{encrypt($Keuangan)}}">
                  <button type="submit" name="button" class="btn btn-primary">Cetak</button>
                </form>
                <form action="{!!route('exportKeuangan')!!}" method="post" class="float-left">
                  @csrf
                  <input type="hidden" name="keuangan" value="{{encrypt($Keuangan)}}">
                  <button type="submit" name="button" class="btn btn-info">Export</button>
                </form>
              </div>
              <div class="col-lg-8 text-center">
                <form action="{!!route('laporanKeuangan')!!}" method="post">
                  @csrf
                  <div class="col-lg-5">
                    <input type="date" name="tanggal_awal" class="form-control" value="{{$request->tanggal_awal??null}}">
                  </div>
                  <div class="col-lg-5">
                    <input type="date" name="tanggal_akhir" class="form-control" value="{{$request->tanggal_akhir??null}}">
                  </div>
                  <div class="col-lg-2">
                    <button type="submit" name="button" class="btn btn-info">Filter</button>
                  </div>
                </form>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable-report" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Tanggal</th>
                    <th>Tipe</th>
                    <th>Sumber</th>
                    <th>Keterangan</th>
                    <th>Jumlah Masuk</th>
                    <th>Jumlah Keluar</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Keuangan as $DataKeuangan)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{HTanggal::FormatDate($DataKeuangan->created_at)}}</td>
                      <td>{{$DataKeuangan->TipeText}}</td>
                      <td>{{$DataKeuangan->SumberText}}</td>
                      <td>{{$DataKeuangan->KeteranganText}}</td>
                      <td>
                        @if ($DataKeuangan->tipe == 1)
                          Rp. {{number_format($DataKeuangan->jumlah)}}
                        @endif
                      </td>
                      <td>
                        @if ($DataKeuangan->tipe == 2)
                          Rp. {{number_format($DataKeuangan->jumlah)}}
                        @endif
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
