@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Laporan Karyawan</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('cetakKaryawan')}}" type="button" name="button" class="btn btn-primary float-left" target="_blank">Cetak</a>
              <form action="{!!route('exportKaryawan')!!}" method="post">
                @csrf
                <input type="hidden" name="karyawan" value="{{encrypt($Karyawan)}}">
                <button type="submit" name="button" class="btn btn-info">Export</button>
              </form>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable-report" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Tempat,Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>No Handphone</th>
                    <th>Jabatan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Karyawan as $Index=>$DataKaryawan)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{$DataKaryawan->nama}}</td>
                      <td>{{$DataKaryawan->TTL}}</td>
                      <td>{!!nl2br($DataKaryawan->alamat)!!}</td>
                      <td>{{$DataKaryawan->no_handphone}}</td>
                      <td>{{$DataKaryawan->Jabatan->nama}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
