@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Laporan Program</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('cetakPendaftaran')}}" type="button" name="button" class="btn btn-primary float-left" target="_blank">Cetak</a>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Peserta</th>
                    <th>Jumlah Program</th>
                    <th>Tanggal Daftar</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Pendaftaran as $DataPendaftaran)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$DataPendaftaran->Peserta->nama}}</td>
                      <td align="center">{{$DataPendaftaran->Program->count()}}</td>
                      <td>{{HTanggal::FormatDate($DataPendaftaran->tanggal_daftar)}}</td>
                      <td align="center">{{$DataPendaftaran->StatusKursus}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
