@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Laporan {{ucfirst($type)}} Peserta</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable-report" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Id Pendaftaran</th>
                    <th>Nama Peserta</th>
                    <th>Jumlah Program</th>
                    <th>Daftar Pendaftaran</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Pendaftaran as $DataPendaftaran)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td class="text-center">{{$DataPendaftaran->id}}</td>
                      <td>{{$DataPendaftaran->Peserta->nama}}</td>
                      <td class="text-center">{{$DataPendaftaran->PendaftaranProgram->count()}}</td>
                      <td>{{HTanggal::FormatDate($DataPendaftaran->created_at)}}</td>
                      <td>
                        <a href="{{Route('cetakSertifikat', ['id' => $DataPendaftaran->UUID, 'type' => $type])}}" class="btn btn-sm btn-round btn-info" data-toggle="tooltip" data-placement="bottom" title="Cetak" target="_blank"><i class="fa fa-print"></i></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
