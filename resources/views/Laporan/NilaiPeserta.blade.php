@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Laporan Nilai Peserta</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('cetakNilaiPeserta')}}" type="button" name="button" class="btn btn-primary float-left" target="_blank">Cetak</a>
              <form action="{!!route('exportNilai')!!}" method="post">
                @csrf
                <input type="hidden" name="nilai" value="{{encrypt($PendaftaranProgram)}}">
                <button type="submit" name="button" class="btn btn-info">Export</button>
              </form>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable-report" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Id Pendaftaran</th>
                    <th>Nama Peserta</th>
                    <th>Nama Program</th>
                    <th>Nilai</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($PendaftaranProgram as $DataPendaftaranProgram)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td class="text-center">{{$DataPendaftaranProgram->Pendaftaran->id}}</td>
                      <td>{{$DataPendaftaranProgram->Pendaftaran->Peserta->nama}}</td>
                      <td>{{$DataPendaftaranProgram->Program->nama}}</td>
                      <td>{{round($DataPendaftaranProgram->Nilai->avg('nilai'),2)?:"Belum Ada Nilai"}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
