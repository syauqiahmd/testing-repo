@extends('Layouts.MasterUser')
@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
          <h3>Laporan Program</h3>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <a href="{{Route('cetakProgram')}}" type="button" name="button" class="btn btn-primary float-left" target="_blank">Cetak</a>
              <form action="{!!route('exportProgram')!!}" method="post">
                @csrf
                <input type="hidden" name="program" value="{{encrypt($Program)}}">
                <button type="submit" name="button" class="btn btn-info">Export</button>
              </form>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable-report" class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama Program</th>
                    <th>Jumlah Pertemuan</th>
                    <th>Biaya</th>
                    <th>Jumlah Peserta</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Program as $Index=>$DataProgram)
                    <tr>
                      <td>{{$Index+1}}</td>
                      <td>{{$DataProgram->nama}}</td>
                      <td class="text-center">{{$DataProgram->jumlah_pertemuan}}</td>
                      <td align="right">Rp. {{number_format($DataProgram->biaya)}}</td>
                      <td>{{$DataProgram->Pendaftaran->count()}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
