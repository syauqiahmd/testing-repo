@extends('Cetak.master')
@section('judul', "Laporan Data Keuangan $tipe")
@section('content')
  <div class="content">
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th>#</th>
          <th>Tanggal</th>
          <th>Keterangan</th>
          <th>Jumlah</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($keuangan as $DataKeuangan)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{HTanggal::FormatDate($DataKeuangan->created_at)}}</td>
            <td>{{$DataKeuangan->KeteranganText}}</td>
            <td align="right">
              Rp. {{number_format($DataKeuangan->jumlah)}}
            </td>
          </tr>
        @endforeach
        <tr>
          <td colspan="3" align="center">Jumlah</td>
          <td align="right">Rp. {{number_format($keuangan->sum('jumlah'))}}</td>
        </tr>
      </tbody>
    </table>
  </div>
@endsection
