@extends('Cetak.master')
@section('judul', 'Daftar Nilai Peserta')
@section('content')
  @foreach ($Pendaftaran->PendaftaranProgram as $PendaftaranProgram)
    <div class="content">
      <table style="width:100%">
        <tbody>
          <tr>
            <td style="width:150px">Id Pendaftaran</td>
            <td>: {{$Pendaftaran->id}}</td>
          </tr>
          <tr>
            <td>Nama</td>
            <td>: {{$Pendaftaran->Peserta->nama}}</td>
          </tr>
          <tr>
            <td>Program</td>
            <td>: {{$PendaftaranProgram->Program->nama}}</td>
          </tr>
        </tbody>
      </table>
      <table class="table" style="margin-top:20px">
        <thead class="thead-light">
          <tr>
            <th>#</th>
            <th>Materi</th>
            <th>Nilai</th>
            <th>Rata-Rata Nilai</th>
          </tr>
        </thead>
        <tbody>
          @unless ($PendaftaranProgram->Program->Materi->count())
            <tr>
              <td colspan="4" class="text-center">
                <h2>Data Materi Tidak Ada</h2>
              </td>
            </tr>
          @endunless
          @foreach ($PendaftaranProgram->Program->Materi as $DataMateri)
            <tr>
              <td class="col-sm-1 text-right">{{$loop->iteration}}</td>
              <td class="col-sm-6">{{$DataMateri->nama}}</td>
              <td class="col-sm-5">{{$PendaftaranProgram->Nilai->where('materi_id', $DataMateri->id)->first()->nilai??"Belum Ada Nilai"}}</td>
              @if ($loop->first)
                <td rowspan="{{$loop->count}}" class="text-center">{{round($PendaftaranProgram->Nilai->avg('nilai'),2)?:"Belum Ada Nilai"}}</td>
              @endif
            </tr>
          @endforeach
        </tbody>
      </table>
      <table style="width:100%; margin-top:30px">
        <tbody>
          <tr>
            <td class="text-center" style="width:50%">
              <strong>Ketua Lembaga,</strong>
              <br>
              <br>
              <br>
              <br>
              <br>
              {{$pengaturan->Karyawan->nama??'(NAMA)'}}
            </td>
            <td class="text-center">
              <strong>Pengajar/Tutor,</strong>
              <br>
              <br>
              <br>
              <br>
              <br>
              {{$PendaftaranProgram->Karyawan->nama??'(NAMA)'}}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    @unless ($loop->last)
      <div class="page-break"></div>
    @endunless
  @endforeach
@endsection
