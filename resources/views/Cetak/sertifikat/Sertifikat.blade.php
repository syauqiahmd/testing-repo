@extends('Cetak.master')
@section('judul', 'Daftar Nilai Peserta')
@section('content')
  @foreach ($Pendaftaran->PendaftaranProgram as $PendaftaranProgram)
    <div class="content">
      <table style="width:100%;">
        <tr>
          <td align="left">
            Nomor Perizinan<br>
            - Nomor Izin Pendirian: 114/Leg/Akta/2017.PN.P11<br>
            - Nomor Iziz Operasional: Nomor 163 Tahun 2017<br>
            - Nomor Pokok Sekolah Nasional (NPSN): K5668149<br>
            - Nomor Reg. DPP HIPKI: 02952
          </td>
          <td align="right" style="vertical-align:top; font-size:14px; font-weight:bold;">
            Nomor Seri Sertifikat:<br>
            {{sprintf("%03d", $Pendaftaran->id)}}-{{$loop->iteration}}/BPC-TALA/SK/IX/2018
          </td>
        </tr>
      </table>
      <table style="width: 100%; margin-top:25px;">
        <tr>
          <td align="center" colspan="2">
            <h1>SERTIFIKAT<br>
            Certificate</h1><br>
            Lembaga Kursus dan Pelatihan [LKP]<br>
            “Bina Pelaihari Computer (BPC)” menerangkan bahwa:<br><br>
            <h1>{{$Pendaftaran->Peserta->nama}}</h1><br>
            Lahir di {{$Pendaftaran->Peserta->tempat_lahir}} pada Tanggal {{HTanggal::FormatDate($Pendaftaran->Peserta->tanggal_lahir)}}<br>
            Dinyatakan LULUS dalam Program Pendidikan dan Pelatihan Komputer<br><br>
            <h1>{{$PendaftaranProgram->Program->nama}}</h1><br>
            Nilai yang Dicapai Tertera pada Daftar  Nilai Dibalik Sertifikat ini.
          </td>
        </tr>
        <tr>
          <td class="text-center" style="width:50%;">
          </td>
          <td class="text-center">
            <br>
            <br>
            <br>
            <br>
            <br>
            <strong>Ketua Lembaga,</strong>
            <br>
            <br>
            <br>
            <br>
            <br>
            {{$pengaturan->Karyawan->nama??'(NAMA)'}}
          </td>
        </tr>
      </table>
    </div>
  @endforeach
@endsection
