@extends('Cetak.master')
@section('judul', 'Laporan Data Program')
@section('content')
  <div class="content">
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th>#</th>
          <th>Nama Program</th>
          <th>Jumlah Pertemuan</th>
          <th>Biaya</th>
          <th>Jumlah Peserta</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($Program as $DataProgram)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$DataProgram->nama}}</td>
            <td class="text-center">{{$DataProgram->jumlah_pertemuan}}</td>
            <td align="right">Rp. {{number_format($DataProgram->biaya)}}</td>
            <td class="text-center">{{$DataProgram->Pendaftaran->count()}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
