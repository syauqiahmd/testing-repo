@extends('Cetak.master')
@section('judul', 'Laporan Data Nilai Peserta')
@section('content')
  <div class="content">
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th>#</th>
          <th>Id Pendaftaran</th>
          <th>Nama Peserta</th>
          <th>Nama Program</th>
          <th>Nilai</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($PendaftaranProgram as $DataPendaftaranProgram)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td class="text-center">{{$DataPendaftaranProgram->Pendaftaran->id}}</td>
            <td>{{$DataPendaftaranProgram->Pendaftaran->Peserta->nama}}</td>
            <td>{{$DataPendaftaranProgram->Program->nama}}</td>
            <td class="text-center">{{round($DataPendaftaranProgram->Nilai->avg('nilai'),2)?:"Belum Ada Nilai"}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
