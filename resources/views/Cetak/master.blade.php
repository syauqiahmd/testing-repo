<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
  body{
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  }
  .table{
    border-collapse: collapse;
    width: 100%;
  }
  .table > * > * > *{
    border: 1px solid #000000;
    padding: 5px;
  }
  .text-center{
    text-align: center;
  }
  .nowrap{
    white-space: nowrap;
  }
  .page-break{
    page-break-after: always;
  }
  .content{
    /* position: relative; */
    font-size: 12px;
  }
  header {
    position: fixed;
    top: 0cm;
    left: 0cm;
    right: 0cm;
    height: 2cm;
  }
  header > * > h2, h1{
    padding: 0;
    margin: 0;
  }
  th{
    text-align: center;
  }
  .plain-text{
    font-size: 12px;
  }
  .company-text {
    font-weight: bold;
    font-size: 16px;
  }
  .kop-surat{
    position: fixed;
  }
  .kop-surat::after {
    display: block;
    content: "";
    clear: both;
    height: 110px;
  }
  </style>
  <title>Laporan Peserta</title>
</head>
<body>
  <table width="100%" class="kop-surat">
    <tr>
      <td align="center" width="20%">
        <img src="../public/img/logo/logobpc.png" height="80px" alt="RSUD">
      </td>
      <td align="center">
        <div class="plain-text">LPK LEMBAGA KURSUS DAN PELATIHAN</div>
        <div class="company-text">"BINA PELAIHARI KOMPUTER(BPC)"</div>
        <div class="plain-text">
          Alamat Jalan Ki Hajar Dewantara RT.07. Angsau Kecamatan Kabupaten Pelaihari KP.70814<br>
          Kabupaten Tanah Laut.- Kalimantan Selatan | Email: pelaihari.computer@gmail.com<br>
          Contact:0852-4867-9207 (Telp) | 0852-4804-5334 (SMS) | 0815-2833-8695 (WA)
        </div>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <hr>
      </td>
    </tr>
  </table>
  @yield('content')
</body>
</html>
