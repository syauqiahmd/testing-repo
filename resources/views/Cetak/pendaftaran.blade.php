@extends('Cetak.master')
@section('judul', 'Laporan Data Pendaftaran')
@section('content')
  <div class="content">
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th>#</th>
          <th>Nama Peserta</th>
          <th>Jumlah Program</th>
          <th>Tanggal Daftar</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($Pendaftaran as $DataPendaftaran)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$DataPendaftaran->Peserta->nama}}</td>
            <td align="center">{{$DataPendaftaran->Program->count()}}</td>
            <td>{{HTanggal::FormatDate($DataPendaftaran->tanggal_daftar)}}</td>
            <td align="center">{{$DataPendaftaran->StatusKursus}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection
