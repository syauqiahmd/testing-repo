$('.btn-delete').click(function(){
  var url = $(this).attr('href')
  var id = $(this).attr('data')
  var status = $(this).attr('status')
  if (!status) {
    swal({
      title   : "Hapus",
      text    : "Yakin Ingin Hapus Data?",
      icon    : "warning",
      buttons : [
        "Batal",
        "Hapus",
      ],
    })
    .then((hapus) => {
      if (hapus) {
        swal({
          title  : "Berhasil",
          text   : "Data Akan dihapus",
          icon   : "success",
          timer  : 2500,
        })
        window.location = url+'/verify/'+id+''
      } else {
        swal({
          title  : "Batal",
          text   : "Data Batal dihapus",
          icon   : "info",
          timer  : 2500,
        })
      }
    })
  }else{
    swal({
      title   : "Hapus",
      text    : status,
      icon    : "warning",
      buttons : "OK",
    })
  }
})

$('#logout').click(function(){
  swal({
    title   : "Logout",
    text    : "Yakin Ingin Keluar?",
    icon    : "warning",
    buttons : [
      "Batal",
      "Logout",
    ],
  })
  .then((logout) => {
    if (logout) {
      swal({
        title  : "Logout",
        text   : "Anda Telah Logout",
        icon   : "success",
        timer  : 2500,
      })
      window.location = "/logout"
    } else {
      swal({
        title  : "Batal Logout",
        text   : "Anda Batal Logout",
        icon   : "info",
        timer  : 2500,
      })
    }
  })
})

window.notif = function(tipe, judul, pesan){
  swal({
    title   : judul,
    text    : pesan,
    icon    : tipe,
  })
}
$(document).ready(function(){
  $(".program-checkbox").click(function(){
    var biaya = parseInt($(this).attr('data-biaya'))
    var biayaCurrent = parseInt($("#total_biaya > .biaya-hidden").val())
    biayaCurrent = $(".program-checkbox[value="+$(this).val()+"]").is(":checked")?(biayaCurrent+biaya):(biayaCurrent-biaya)
    $("#total_biaya > .biaya-hidden").val(biayaCurrent)
    $("#total_biaya > .biaya-show").val("Rp. "+biayaCurrent.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
    return hitungBayar()
  })

  $("#total_bayar").keyup(function(){
    return hitungBayar()
  })

  function hitungBayar(){
    var biayaCurrent = parseInt($("#total_biaya > .biaya-hidden").val())
    var totalBayar = parseInt($("#total_bayar").val())
    $("#total_bayar").val(totalBayar>biayaCurrent? biayaCurrent:totalBayar)
    var sisaBayar = biayaCurrent>totalBayar? biayaCurrent-totalBayar:0
    $("#sisa_bayar").val("Rp. "+sisaBayar.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
  }

  $(".datatable-form").dataTable({
    pageLength: 5,
    lengthChange: false,
    "columns": [
      { "searchable": false },
      null,
      { "searchable": false },
      { "searchable": false },
    ],
  })

  $("#datatable-report").dataTable({
    pageLength: 10,
    lengthChange: false,
    searching: false,
    ordering: false,
  })

  $(".jadwal-checkbox").change(function(){
    var program = $(this).attr('data-program')
    var checkbox = $("*[data-program="+program+"]")
    if(checkbox.is(':checked')) {
      checkbox.removeAttr('required')
    } else {
      checkbox.attr('required', 'required')
    }
  })

  $("#jadwalSubmit").click(function(){
    if($(".jadwal-checkbox[required]").length){
      notif("warning", "Kesalahan", "Harap Isi Jadwal Minimal 1 Tiap Program")
      return false
    }
  })

  $(".select2").select2()

  $("#karyawanIdGaji, #periodeGaji").change(function(){
    console.log('asd');
    $("[type='submit']").removeAttr("disabled", false)
    $("#statusGaji").val("")
    $("#gajiKaryawan").val("Rp. 0")
    if ($("#karyawanIdGaji").val()) {
      $("#gajiKaryawan").val("Rp. "+$("#karyawanIdGaji").find(':selected').attr('data-gaji'))
      axios({
        method: 'get',
        url: '/api/data/karyawan/'+$("#karyawanIdGaji").val()+'/keuangan',
      }).then((response) => {
        $.each(response.data.keuangan, function (index,value){
          if (value.periode_gaji == $("#periodeGaji").val()) {
            $("[type='submit']").attr("disabled", true)
            $("#statusGaji").val("Gaji Periode Tersebut Sudah Pernah Dibayarkan")
          }else{
            $("#statusGaji").val("Gaji Belum Dibayarkan")
          }
        })
      })
    }
  })
  $(".btn-input").click(function(){
    var status = $(this).attr('status')
    var dataIndex = parseInt($(this).parent().attr('data-index'))
    if (status == "add") {
      if (!$(this).parent().find("input").val()) {
        notif('warning', 'Peringatan', 'Isi Teks Sebelum Menambahkan')
        return
      }
      $(".input-dupli[data-index="+dataIndex+"]").clone(true).insertAfter(".input-dupli:last").attr('data-index', dataIndex+1).find("input").val("");
      $(".btn-input:not(:last) > i").removeClass("fa-plus")
      $(".btn-input:not(:last) > i").addClass("fa-minus")
      $(this).attr('status', 'remove')
    }else{
      $(".input-dupli[data-index="+dataIndex+"]").remove()
    }
  })
})
