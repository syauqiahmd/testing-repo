<?php
Route::POST('login', 'Auth\LoginController@login')->name('Login');
Route::GET('login', 'Auth\LoginController@showLoginForm')->name('LoginForm');
Route::GET('logout', 'Auth\LoginController@logout')->name('Logout');

Route::get('', 'HomeController@index')->name('Dashboard');

Route::Group(['middleware' => ['AuthMiddleware']], function(){
  Route::Group(['middleware' => ['PesertaMiddleware']], function(){
    Route::group(['prefix' => 'kursus', 'as' => 'kursus'], function () {
      Route::GET('', 'Peserta\KursusController@Data')->name('Data');
      Route::GET('{id}/info', 'Peserta\KursusController@Info')->name('Info');
    });
  });

  Route::Group(['middleware' => ['KaryawanMiddleware']], function(){
    Route::group(['prefix' => 'pengaturan', 'as' => 'pengaturan'], function () {
      Route::get('', 'PengaturanController@pengaturan');
      Route::post('', 'PengaturanController@pengaturanSubmit')->name('Submit');
    });
    Route::group(['prefix' => 'jabatan', 'as' => 'jabatan'], function () {
      Route::GET('', 'JabatanController@Data')->name('Data');
      Route::GET('tambah', 'JabatanController@TambahForm')->name('TambahForm');
      Route::POST('tambah', 'JabatanController@TambahSubmit')->name('TambahSubmit');
      Route::GET('edit/{id}', 'JabatanController@EditForm')->name('EditForm');
      Route::POST('edit/{id}', 'JabatanController@EditSubmit')->name('EditSubmit');
      Route::GET('hapus/{id?}/{verify?}', 'JabatanController@Hapus')->name('Hapus');
    });
    Route::group(['prefix' => 'user', 'as' => 'user'], function () {
      Route::group(['prefix' => 'karyawan', 'as' => 'Karyawan'], function () {
        Route::GET('', 'KaryawanController@Data')->name('Data');
        Route::GET('tambah', 'KaryawanController@TambahForm')->name('TambahForm');
        Route::POST('tambah', 'KaryawanController@TambahSubmit')->name('TambahSubmit');
        Route::GET('edit/{id}', 'KaryawanController@EditForm')->name('EditForm');
        Route::POST('edit/{id}', 'KaryawanController@EditSubmit')->name('EditSubmit');
        Route::GET('hapus/{id?}/{verify?}', 'KaryawanController@Hapus')->name('Hapus');
      });
      Route::group(['prefix' => 'peserta', 'as' => 'Peserta'], function () {
        Route::GET('', 'PesertaController@Data')->name('Data');
        Route::GET('tambah', 'PesertaController@TambahForm')->name('TambahForm');
        Route::POST('tambah', 'PesertaController@TambahSubmit')->name('TambahSubmit');
        Route::GET('edit/{id}', 'PesertaController@EditForm')->name('EditForm');
        Route::POST('edit/{id}', 'PesertaController@EditSubmit')->name('EditSubmit');
        Route::GET('hapus/{verify?}/{id?}', 'PesertaController@Hapus')->name('Hapus');
      });
    });
    Route::group(['prefix' => 'program', 'as' => 'program'], function () {
      Route::GET('', 'ProgramController@Data')->name('Data');
      Route::GET('tambah', 'ProgramController@TambahForm')->name('TambahForm');
      Route::POST('tambah', 'ProgramController@TambahSubmit')->name('TambahSubmit');
      Route::GET('edit/{id}', 'ProgramController@EditForm')->name('EditForm');
      Route::POST('edit/{id}', 'ProgramController@EditSubmit')->name('EditSubmit');
      Route::GET('hapus/{verify?}/{id?}', 'ProgramController@Hapus')->name('Hapus');
    });
    Route::GROUP(['prefix' => 'program', 'as' => 'program'], function () {
      Route::GET('', 'ProgramController@Data')->name('Data');
      Route::GET('tambah', 'ProgramController@TambahForm')->name('TambahForm');
      Route::POST('tambah', 'ProgramController@TambahSubmit')->name('TambahSubmit');
      Route::GET('edit/{id}', 'ProgramController@EditForm')->name('EditForm');
      Route::POST('edit/{id}', 'ProgramController@EditSubmit')->name('EditSubmit');
      Route::GET('hapus/{verify?}/{id?}', 'ProgramController@Hapus')->name('Hapus');
      Route::GROUP(['prefix' => '{idProgram}/jadwal', 'as' => 'Jadwal'], function () {
        Route::GET('', 'JadwalController@Data')->name('Data');
        Route::GET('tambah', 'JadwalController@TambahForm')->name('TambahForm');
        Route::POST('tambah', 'JadwalController@TambahSubmit')->name('TambahSubmit');
        Route::GET('edit/{id}', 'JadwalController@EditForm')->name('EditForm');
        Route::POST('edit/{id}', 'JadwalController@EditSubmit')->name('EditSubmit');
        Route::GET('hapus/{verify?}/{id?}', 'JadwalController@Hapus')->name('Hapus');
      });
      Route::GROUP(['prefix' => '{idProgram}/materi', 'as' => 'Materi'], function () {
        Route::GET('', 'MateriController@Data')->name('Data');
        Route::GET('tambah', 'MateriController@TambahForm')->name('TambahForm');
        Route::POST('tambah', 'MateriController@TambahSubmit')->name('TambahSubmit');
        Route::GET('edit/{id}', 'MateriController@EditForm')->name('EditForm');
        Route::POST('edit/{id}', 'MateriController@EditSubmit')->name('EditSubmit');
        Route::GET('hapus/{verify?}/{id?}', 'MateriController@Hapus')->name('Hapus');
      });
    });
    Route::group(['prefix' => 'pendaftaran', 'as' => 'pendaftaran'], function () {
      Route::GET('', 'PendaftaranController@Data')->name('Data');
      Route::GET('tambah', 'PendaftaranController@TambahForm')->name('TambahForm');
      Route::POST('tambah', 'PendaftaranController@TambahSubmit')->name('TambahSubmit');
      Route::GET('jadwal/{id}', 'PendaftaranController@JadwalForm')->name('JadwalForm');
      Route::POST('jadwal/{id}', 'PendaftaranController@JadwalSubmit')->name('JadwalSubmit');
      Route::GET('info/{id}', 'PendaftaranController@Info')->name('Info');
      Route::GET('hapus/{verify?}/{id?}', 'PendaftaranController@Hapus')->name('Hapus');
    });
    Route::group(['prefix' => 'nilai', 'as' => 'nilai'], function () {
      Route::GET('', 'NilaiController@Data')->name('Data');
      Route::GET('detail/{id}', 'NilaiController@Detail')->name('Detail');
      Route::GET('edit/{id}', 'NilaiController@Edit')->name('Edit');
      Route::POST('edit/{id}', 'NilaiController@EditSubmit')->name('EditSubmit');
    });

    Route::group(['prefix' => 'keuangan', 'as' => 'keuangan'], function () {
      Route::GET('', 'KeuanganController@Data')->name('Data');
      Route::group(['prefix' => 'angsuran', 'as' => 'Angsuran'], function () {
        Route::GET('', 'AngsuranController@Data')->name('Data');
        Route::GET('info/{id}', 'AngsuranController@Info')->name('Info');
        Route::GET('bayar/{id}', 'AngsuranController@Bayar')->name('Bayar');
        Route::POST('bayar/{id}', 'AngsuranController@BayarSubmit')->name('BayarSubmit');
      });
      Route::group(['prefix' => 'jasa', 'as' => 'Jasa'], function () {
        Route::GET('', 'JasaController@Data')->name('Data');
        Route::GET('tambah', 'JasaController@TambahForm')->name('TambahForm');
        Route::POST('tambah', 'JasaController@TambahSubmit')->name('TambahSubmit');
        Route::GET('edit/{id}', 'JasaController@EditForm')->name('EditForm');
        Route::POST('edit/{id}', 'JasaController@EditSubmit')->name('EditSubmit');
        Route::GET('hapus/{verify?}/{id?}', 'JasaController@Hapus')->name('Hapus');
      });
      Route::group(['prefix' => 'operasional', 'as' => 'Operasional'], function () {
        Route::GET('', 'OperasionalController@Data')->name('Data');
        Route::GET('tambah', 'OperasionalController@TambahForm')->name('TambahForm');
        Route::POST('tambah', 'OperasionalController@TambahSubmit')->name('TambahSubmit');
        Route::GET('edit/{id}', 'OperasionalController@EditForm')->name('EditForm');
        Route::POST('edit/{id}', 'OperasionalController@EditSubmit')->name('EditSubmit');
        Route::GET('hapus/{verify?}/{id?}', 'OperasionalController@Hapus')->name('Hapus');
      });
      Route::group(['prefix' => 'gaji', 'as' => 'Gaji'], function () {
        Route::GET('', 'GajiController@Data')->name('Data');
        Route::GET('tambah', 'GajiController@TambahForm')->name('TambahForm');
        Route::POST('tambah', 'GajiController@TambahSubmit')->name('TambahSubmit');
        Route::GET('hapus/{verify?}/{id?}', 'GajiController@Hapus')->name('Hapus');
      });
    });
    Route::group(['prefix' => 'laporan', 'as' => 'laporan'], function () {
      Route::GET('pendaftaran', 'LaporanController@pendaftaran')->name('Pendaftaran');
      Route::GET('peserta', 'LaporanController@Peserta')->name('Peserta');
      Route::GET('program', 'LaporanController@Program')->name('Program');
      Route::GET('nilaipeserta', 'LaporanController@NilaiPeserta')->name('NilaiPeserta');
      Route::GET('karyawan', 'LaporanController@Karyawan')->name('Karyawan');
      Route::GET('sertifikat/{type?}', 'LaporanController@Sertifikat')->name('Sertifikat');
      Route::group(['prefix' => 'keuangan', 'as' => 'Keuangan'], function () {
        Route::match(['get', 'post'], '', 'LaporanController@Keuangan');
        Route::match(['get', 'post'], '{tipe}', 'LaporanController@keuanganSpecific')->name('Specific');
      });
    });
    Route::group(['prefix' => 'cetak', 'as' => 'cetak'], function () {
      Route::GET('pendaftaran', 'LaporanController@cetakPendaftaran')->name('Pendaftaran');
      Route::GET('peserta', 'LaporanController@cetakPeserta')->name('Peserta');
      Route::GET('program', 'LaporanController@cetakProgram')->name('Program');
      Route::GET('nilaipeserta', 'LaporanController@cetakNilaiPeserta')->name('NilaiPeserta');
      Route::GET('karyawan', 'LaporanController@cetakKaryawan')->name('Karyawan');
      Route::GET('sertifikat/{id}/{type?}', 'LaporanController@cetakSertifikat')->name('Sertifikat');
      Route::group(['prefix' => 'keuangan', 'as' => 'Keuangan'], function () {
        Route::post('', 'LaporanController@cetakKeuangan');
        Route::post('{tipe}', 'LaporanController@cetakKeuanganSpecific')->name('Specific');
      });
    });
    Route::group(['prefix' => 'export', 'as' => 'export'], function () {
      Route::post('peserta', 'ExportController@peserta')->name('Peserta');
      Route::post('program', 'ExportController@program')->name('Program');
      Route::post('nilai', 'ExportController@nilai')->name('Nilai');
      Route::post('karyawan', 'ExportController@karyawan')->name('Karyawan');
      Route::group(['prefix' => 'keuangan', 'as' => 'Keuangan'], function () {
        Route::post('', 'ExportController@keuangan');
        Route::post('{tipe}', 'ExportController@keuanganSpecific')->name('Specific');
      });
    });
  });
});



Route::get('/home', 'HomeController@index')->name('home');
