<?php

use Illuminate\Database\Seeder;
use App\Karyawan;
use App\User;

class UsersTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    if (!User::where('username', 'admin')->count()) {
      $faker = Faker\Factory::create('id_ID');
      $User = new User;
      $User->username = 'admin';
      $User->password = 'admin';
      $User->tipe = 1;
      $User->save();
      $Karyawan = new Karyawan;
      $Karyawan->nama = $faker->name;
      $Karyawan->tempat_lahir = $faker->city;
      $Karyawan->tanggal_lahir = $faker->dateTime('-18 years');
      $Karyawan->alamat = $faker->address;
      $Karyawan->no_handphone = $faker->phoneNumber;
      $Karyawan->jabatan_id = $faker->numberBetween(1,2);
      $Karyawan->gaji = $faker->numerify('####000');
      $Karyawan->user_id = $User->id;
      $Karyawan->save();
    }
  }
}
