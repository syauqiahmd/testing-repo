<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSyaratToPengaturansTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('pengaturans', function (Blueprint $table) {
      $table->text('syarat')->after('alamat');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('pengaturans', function (Blueprint $table) {
      $table->dropColumn('syarat');
    });
  }
}
