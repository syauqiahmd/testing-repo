<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeriodeGajiToKeuangansTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('keuangans', function (Blueprint $table) {
      $table->date('periode_gaji')->nullable()->after('karyawan_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('keuangans', function (Blueprint $table) {
      $table->dropColumn('periode_gaji');
    });
  }
}
