<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPendaftaranJadwalProgramTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::dropIfExists('pendaftaran_jadwal_program');
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::create('pendaftaran_jadwal_program', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('pendaftaran_id');
      $table->integer('jadwal_program_id');
      $table->timestamps();
    });
  }
}
