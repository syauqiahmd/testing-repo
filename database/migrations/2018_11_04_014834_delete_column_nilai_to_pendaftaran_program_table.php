<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteColumnNilaiToPendaftaranProgramTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('pendaftaran_program', function (Blueprint $table) {
      $table->dropColumn('nilai');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('pendaftaran_program', function (Blueprint $table) {
      $table->integer('nilai');
    });
  }
}
