<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeteranganToKeuangansTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('keuangans', function (Blueprint $table) {
      $table->string('keterangan')->nullable()->after('pendaftaran_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('keuangans', function (Blueprint $table) {
      $table->dropColumn('keterangan');
    });
  }
}
