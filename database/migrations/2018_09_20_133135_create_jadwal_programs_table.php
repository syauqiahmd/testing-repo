<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalProgramsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('jadwal_programs', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('hari');
      $table->time('jam_awal');
      $table->time('jam_akhir');
      $table->integer('program_id');
      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('jadwal_programs');
  }
}
