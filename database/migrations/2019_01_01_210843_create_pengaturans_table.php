<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengaturansTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('pengaturans', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('karyawan_id')->comment('Ketua');
      $table->string('nama')->comment('Nama Lembaga');
      $table->string('alamat')->comment('Alamat Lembaga');
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('pengaturans');
  }
}
