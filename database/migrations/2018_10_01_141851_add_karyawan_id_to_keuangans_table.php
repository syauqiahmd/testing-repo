<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKaryawanIdToKeuangansTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('keuangans', function (Blueprint $table) {
      $table->integer('karyawan_id')->default(0)->after('pendaftaran_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('keuangans', function (Blueprint $table) {
      $table->dropColumn('karyawan_id');
    });
  }
}
