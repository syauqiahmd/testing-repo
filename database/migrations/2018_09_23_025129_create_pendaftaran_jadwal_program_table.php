<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendaftaranJadwalProgramTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('pendaftaran_jadwal_program', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('pendaftaran_id');
      $table->integer('jadwal_program_id');
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('pendaftaran_jadwal_program');
  }
}
