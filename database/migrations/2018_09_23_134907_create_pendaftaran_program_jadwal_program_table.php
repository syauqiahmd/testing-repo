<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendaftaranProgramJadwalProgramTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('pendaftaran_program_jadwal_program', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('pendaftaran_program_id');
      $table->integer('jadwal_program_id');
      $table->timestamps();
    });
  }
  
  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('pendaftaran_program_jadwal_program');
  }
}
