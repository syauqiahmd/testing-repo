<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGajiToKaryawansTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('karyawans', function (Blueprint $table) {
      $table->double('gaji')->after('no_handphone');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('karyawans', function (Blueprint $table) {
      $table->dropColumn('gaji');
    });
  }
}
