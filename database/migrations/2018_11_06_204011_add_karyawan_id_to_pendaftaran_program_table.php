<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKaryawanIdToPendaftaranProgramTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('pendaftaran_program', function (Blueprint $table) {
      $table->integer('karyawan_id')->nullable()->after('program_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('pendaftaran_program', function (Blueprint $table) {
      $table->dropColumn('karyawan_id');
    });
  }
}
