<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeuangansTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('keuangans', function (Blueprint $table) {
      $table->increments('id');
      $table->tinyInteger('tipe')->comment('Pemasukan(1)/Pengeluaran(2)');
      $table->tinyInteger('sumber')->comment('(1)Kursus/(2)Jasa/(3)Gaji/(4)Operasional');
      $table->double('jumlah');
      $table->integer('pendaftaran_id')->default(0);
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('keuangans');
  }
}
