<?php
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
  return [
    'username' => $faker->unique()->userName,
    'password' => 'rahasia',
    'remember_token' => str_random(10),
  ];
});

$factory->state(User::class, 'karyawan', function (\Faker\Generator $faker) {
  return [
    'tipe' => 1,
  ];
});

$factory->state(User::class, 'peserta', function (\Faker\Generator $faker) {
  return [
    'tipe' => 2,
  ];
});
