<?php

use Faker\Generator as Faker;

$factory->define(Peserta::class, function (Faker $faker) {
  return [
    'nama' => $faker->name,
    'tempat_lahir' => $faker->city,
    'tanggal_lahir' => $faker->dateTime('-18 years'),
    'alamat' => $faker->address,
    'no_handphone' => $faker->phoneNumber,
    'user_id' => factory(User::class)->states('peserta')->create()->id,
  ];
});
