<?php

use Faker\Generator as Faker;

$factory->define(Karyawan::class, function (Faker $faker){
  return [
    'nama' => $faker->name,
    'tempat_lahir' => $faker->city,
    'tanggal_lahir' => $faker->dateTime('-18 years'),
    'alamat' => $faker->address,
    'no_handphone' => $faker->phoneNumber,
    'gaji' => $faker->numerify('###000'),
    'jabatan_id' => $faker->numberBetween(1,2),
    'user_id' => factory(User::class)->states('karyawan')->create()->id,
  ];
});
