<?php

use Faker\Generator as Faker;

$factory->define(Program::class, function (Faker $faker) {
  return [
    'nama' => $faker->safeColorName,
    'jumlah_pertemuan' => $faker->randomDigitNotNull,
    'biaya' => $faker->numerify('###000'),
  ];
});
