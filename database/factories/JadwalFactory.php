<?php

use Faker\Generator as Faker;

$factory->define(JadwalProgram::class, function (Faker $faker) {
  $ProgramId = Program::all()->pluck('id');
  $Jam = $faker->time;
  return [
    'hari' => $faker->numberBetween(1,7),
    'jam_awal' => $Jam,
    'jam_akhir' => $faker->time,
    'program_id' => $faker->randomElement($ProgramId),
  ];
});
