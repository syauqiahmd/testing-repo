<?php

use Faker\Generator as Faker;

$factory->define(Jabatan::class, function (Faker $faker) {
  return [
    'nama' => $faker->jobTitle,
  ];
});
