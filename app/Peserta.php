<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use HTanggal;
use HCrypt;

class Peserta extends Model
{
  use SoftDeletes;

  protected $fillable = ['nama', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'no_handphone', 'user_id'];

  public function User(){
    return $this->belongsTo('App\User');
  }

  public function getTTLAttribute($value){
    $Tanggal = HTanggal::FormatDate($this->tanggal_lahir);
    return "{$this->tempat_lahir}, {$Tanggal}";
  }

  public function getUUIDAttribute($value){
    return HCrypt::Encrypt($this->id);
  }

  public function Pendaftaran(){
    return $this->hasMany('App\Pendaftaran');
  }
}
