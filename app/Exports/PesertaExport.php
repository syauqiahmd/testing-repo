<?php

namespace App\Exports;

use App\Peserta;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PesertaExport implements FromView
{
  private $data;
  
  public function __construct($data){
    $this->data = $data;
  }
  public function view(): View{
    return view('exports.peserta', [
      'Peserta' => $this->data
    ]);
  }
}
