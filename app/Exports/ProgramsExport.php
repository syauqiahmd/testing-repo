<?php

namespace App\Exports;

use App\Program;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProgramsExport implements FromView
{
  private $data;

  public function __construct($data){
    $this->data = $data;
  }
  public function view(): View{
    return view('exports.program', [
      'Program' => $this->data
    ]);
  }
}
