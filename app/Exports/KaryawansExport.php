<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class KaryawansExport implements FromView
{
  private $data;

  public function __construct($data){
    $this->data = $data;
  }
  public function view(): View{
    return view('exports.karyawan', [
      'Karyawan' => $this->data
    ]);
  }
}
