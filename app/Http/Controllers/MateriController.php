<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Materi;
use HCrypt;

class MateriController extends Controller
{
  public function Data($idProgram){
    $idProgram = HCrypt::Decrypt($idProgram);
    $program = Program::findOrFail($idProgram);
    $materi = Materi::whereProgramId($idProgram)->get();
    return view('Materi.Data', compact('program','materi'));
  }

  public function TambahForm($idProgram){
    $idProgram = HCrypt::Decrypt($idProgram);
    $program = Program::findOrFail($idProgram);
    return view('Materi.Tambah', compact('program'));
  }

  public function TambahSubmit(Request $request, $idProgram){
    $idProgram = HCrypt::Decrypt($idProgram);
    $program = Program::findOrFail($idProgram);
    $materi = new Materi;
    $materi->fill($request->all());
    $materi->program_id = $idProgram;
    $materi->save();
    return redirect()->route('programMateriData', ['idProgram' => $program->UUID])->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambahkan']);
  }

  public function EditForm($idProgram, $id){
    $idProgram = HCrypt::Decrypt($idProgram);
    $id = HCrypt::Decrypt($id);
    $program = Program::findOrFail($idProgram);
    $materi = Materi::findOrFail($id);
    return view('Materi.Edit', compact('program', 'materi'));
  }

  public function EditSubmit(Request $request, $idProgram, $id){
    $idProgram = HCrypt::Decrypt($idProgram);
    $id = HCrypt::Decrypt($id);
    $program = Program::findOrFail($idProgram);
    $materi = Materi::findOrFail($id);
    $materi->fill($request->all());
    $materi->save();
    return redirect()->route('programMateriData', ['idProgram' => $program->UUID])->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Diubah']);
  }

  public function Hapus($idProgram, $Verify=null, $Id=null){
    if ($Verify) {
      $idProgram = HCrypt::Decrypt($idProgram);
      $program = Program::findOrFail($idProgram);
      $Id = HCrypt::Decrypt($Id);
      $materi = Materi::findOrFail($Id);
      $materi->delete();
      return redirect()->route('programMateriData', ['idProgram' => $program->UUID])->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Dihapus']);
    }
    return abort(404);
  }
}
