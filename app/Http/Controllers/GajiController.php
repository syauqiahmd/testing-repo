<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Keuangan;
use App\Karyawan;
use HCrypt;

class GajiController extends Controller
{
  public function Data(){
    $Keuangan = Keuangan::whereSumber(4)->orderBy('created_at', 'desc')->get();
    return view('Keuangan.Gaji.Data', compact('Keuangan'));
  }

  public function TambahForm(){
    $Karyawan = Karyawan::all();
    return view('Keuangan.Gaji.Tambah', compact('Karyawan'));
  }

  public function TambahSubmit(Request $request){
    $Karyawan = Karyawan::findOrFail($request->karyawan_id);
    $Keuangan = new Keuangan;
    $Keuangan->tipe = 2;
    $Keuangan->sumber = 4;
    $Keuangan->jumlah = $Karyawan->gaji;
    $Keuangan->fill($request->all());
    $Keuangan->save();
    return redirect()->route('keuanganGajiData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambahkan']);
  }

  public function Hapus($Verify=null, $Id=null){
    if ($Verify) {
      $Id = HCrypt::Decrypt($Id);
      $Keuangan = Keuangan::findOrFail($Id);
      $Keuangan->delete();
      return redirect()->route('keuanganGajiData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Dihapus']);
    }
  }
}
