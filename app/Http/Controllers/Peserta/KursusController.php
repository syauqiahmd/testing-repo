<?php

namespace App\Http\Controllers\Peserta;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pendaftaran;
use HCrypt;
use Auth;

class KursusController extends Controller
{
  public function Data(){
    $Pendaftaran = Pendaftaran::wherePesertaId(Auth::User()->Data->id)->get();
    return view('Peserta.Kursus.Data', compact('Pendaftaran'));
  }

  public function Info($Id){
    $Id = HCrypt::Decrypt($Id);
    $Pendaftaran = Pendaftaran::findOrFail($Id);
    return view('Peserta.Kursus.Info', compact('Pendaftaran'));
  }
}
