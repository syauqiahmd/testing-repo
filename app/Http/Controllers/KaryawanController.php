<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Karyawan;
use App\Jabatan;
use Validator;
use App\User;
use HCrypt;

class KaryawanController extends Controller
{
  public function Data(){
    $Karyawan = Karyawan::all();
    return view('User.Karyawan.Data', compact('Karyawan'));
  }

  public function TambahForm(){
    $Jabatan = Jabatan::all();
    return view('User.Karyawan.Tambah', compact('Jabatan'));
  }

  public function TambahSubmit(Request $request){
    Validator::make($request->all(),[
      'username' => Rule::unique('users'),
    ])->validate();
    $User = new User;
    $User->fill($request->all());
    $User->tipe = 1;
    $User->save();
    $Karyawan = new Karyawan;
    $Karyawan->fill($request->all());
    $Karyawan->user_id = $User->id;
    $Karyawan->save();
    return redirect()->route('userKaryawanData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambahkan']);
  }

  public function EditForm($Id){
    $Id = HCrypt::Decrypt($Id);
    $Karyawan = Karyawan::findOrFail($Id);
    $Jabatan = Jabatan::all();
    return view('User.Karyawan.Edit', compact('Karyawan', 'Jabatan'));
  }

  public function EditSubmit(Request $request, $Id){
    $Id = HCrypt::Decrypt($Id);
    $Karyawan = Karyawan::findOrFail($Id);
    $User = User::findOrFail($Karyawan->user_id);
    Validator::make($request->all(),[
      'username' => Rule::unique('users')->ignore($Karyawan->user_id),
    ])->validate();
    $User->fill($request->all());
    $Karyawan->fill($request->all());
    $User->save();
    $Karyawan->save();
    return redirect()->route('userKaryawanData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Diubah']);
  }

  public function Hapus($Verify=null,$Id=null){
    if ($Verify) {
      $Id = HCrypt::Decrypt($Id);
      $Karyawan = Karyawan::findOrFail($Id);
      $User = User::findOrFail($Karyawan->user_id);
      $User->delete();
      $Karyawan->delete();
      return redirect()->route('userKaryawanData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Dihapus']);
    }
  }
}
