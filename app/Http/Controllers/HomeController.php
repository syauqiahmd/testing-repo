<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Pengaturan;
use App\Keuangan;
use App\Karyawan;
use App\Peserta;
use App\Program;
use Auth;

class HomeController extends Controller
{
  public function index(){
    if (!Auth::User()) return $this->landingPage();
    if (Auth::User()->tipe == 1) {
      return $this->DashboardKaryawan();
    }
    return $this->DashboardPeserta();
  }

  private function DashboardPeserta(){
    $Pendaftaran = Pendaftaran::wherePesertaId(Auth::User()->Data->id)->orderBy('id', 'desc')->get();
    return view('Dashboard.Peserta', compact('Pendaftaran'));
  }

  private function DashboardKaryawan(){
    $Pendaftaran = Pendaftaran::count();
    $Karyawan = Karyawan::count();
    $Peserta = Peserta::count();
    $Program = Program::all();
    $Program = Program::all();
    $Keuangan = new Keuangan;
    return view('Dashboard.Karyawan', compact('Pendaftaran', 'Karyawan', 'Peserta', 'Program', 'Keuangan'));
  }

  private function landingPage(){
    $pengaturan = Pengaturan::first();
    $program = Program::all();
    return view('Dashboard.landingPage', compact('pengaturan', 'program'));
  }
}
