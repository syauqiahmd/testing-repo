<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Karyawan;

class ApiController extends Controller
{
  public function DataKaryawan($Id=null, $with=null){
    $Karyawan = new Karyawan;
    if ($with) $Karyawan = $Karyawan->with($with);
    return $Id? $Karyawan->findOrFail($Id):$Karyawan->all();
  }
}
