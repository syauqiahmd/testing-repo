<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jabatan;
use HCrypt;

class JabatanController extends Controller
{
  public function Data(){
    $Jabatan = Jabatan::all();
    return view('Jabatan.Data', compact('Jabatan'));
  }

  public function TambahForm(){
    return view('Jabatan.Tambah');
  }

  public function TambahSubmit(Request $request){
    $Jabatan = new Jabatan;
    $Jabatan->fill($request->all());
    $Jabatan->save();
    return redirect()->route('jabatanData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambahkan']);
  }

  public function EditForm($Id){
    $Id = HCrypt::Decrypt($Id);
    $Jabatan = Jabatan::findOrFail($Id);
    return view('Jabatan.Edit', compact('Jabatan'));
  }

  public function EditSubmit(Request $request, $Id){
    $Id = HCrypt::Decrypt($Id);
    $Jabatan = Jabatan::findOrFail($Id);
    $Jabatan->fill($request->all());
    $Jabatan->save();
    return redirect()->route('jabatanData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Diubah']);
  }

  public function Hapus($Verify=null,$Id=null){
    if ($Verify) {
      $Id = HCrypt::Decrypt($Id);
      $Jabatan = Jabatan::findOrFail($Id);
      $Jabatan->delete();
      return redirect()->route('jabatanData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Dihapus']);;
    }
  }
}
