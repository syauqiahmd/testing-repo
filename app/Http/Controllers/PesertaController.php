<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Peserta;
use Validator;
use App\User;
use HCrypt;

class PesertaController extends Controller
{
  public function Data(){
    $Peserta = Peserta::all();
    return view('User.Peserta.Data', compact('Peserta'));
  }

  public function TambahForm(){
    return view('User.Peserta.Tambah');
  }

  public function TambahSubmit(Request $request){
    Validator::make($request->all(),[
      'username' => Rule::unique('users'),
    ])->validate();
    $User = new User;
    $User->fill($request->all());
    $User->save();
    $Peserta = new Peserta;
    $Peserta->fill($request->all());
    $Peserta->user_id = $User->id;
    $Peserta->save();
    return redirect()->route('userPesertaData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambahkan']);
  }

  public function EditForm($Id){
    $Id = HCrypt::Decrypt($Id);
    $Peserta = Peserta::findOrFail($Id);
    return view('User.Peserta.Edit', compact('Peserta'));
  }

  public function EditSubmit(Request $request, $Id){
    $Id = HCrypt::Decrypt($Id);
    $Peserta = Peserta::findOrFail($Id);
    $User = User::findOrFail($Peserta->user_id);
    Validator::make($request->all(),[
      'username' => Rule::unique('users')->ignore($Peserta->user_id),
    ])->validate();
    $User->fill($request->all());
    $Peserta->fill($request->all());
    $User->save();
    $Peserta->save();
    return redirect()->route('userPesertaData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Diubah']);
  }

  public function Hapus($Verify=null,$Id=null){
    if ($Verify) {
      $Id = HCrypt::Decrypt($Id);
      $Peserta = Peserta::findOrFail($Id);
      $User = User::findOrFail($Peserta->user_id);
      $Peserta->delete();
      $User->delete();
      return redirect()->route('userPesertaData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Dihapus']);
    }
  }
}
