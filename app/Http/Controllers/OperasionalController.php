<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Keuangan;
use HCrypt;

class OperasionalController extends Controller
{
  public function Data(){
    $Keuangan = Keuangan::whereSumber(3)->orderBy('created_at', 'desc')->get();
    return view('Keuangan.Operasional.Data', compact('Keuangan'));
  }

  public function TambahForm(){
    return view('Keuangan.Operasional.Tambah');
  }

  public function TambahSubmit(Request $request){
    $Keuangan = new Keuangan;
    $Keuangan->tipe = 2;
    $Keuangan->sumber = 3;
    $Keuangan->fill($request->all());
    $Keuangan->save();
    return redirect()->route('keuanganOperasionalData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambahkan']);
  }

  public function EditForm($Id){
    $Id = HCrypt::Decrypt($Id);
    $Keuangan = Keuangan::findOrFail($Id);
    return view('Keuangan.Operasional.Edit', compact('Keuangan'));
  }

  public function EditSubmit(Request $request, $Id){
    $Id = HCrypt::Decrypt($Id);
    $Keuangan = Keuangan::findOrFail($Id);
    $Keuangan->fill($request->all());
    $Keuangan->save();
    return redirect()->route('keuanganOperasionalData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Diubah']);
  }

  public function Hapus($Verify=null, $Id=null){
    if ($Verify) {
      $Id = HCrypt::Decrypt($Id);
      $Keuangan = Keuangan::findOrFail($Id);
      $Keuangan->delete();
      return redirect()->route('keuanganOperasionalData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Dihapus']);
    }
    return abort(404);
  }
}
