<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JadwalProgram;
use App\Program;
use HCrypt;

class JadwalController extends Controller
{
  public function Data($idProgram){
    $idProgram = HCrypt::Decrypt($idProgram);
    $Program = Program::findOrFail($idProgram);
    $Jadwal = JadwalProgram::whereProgramId($idProgram)->get();
    return view('JadwalProgram.Data', compact('Jadwal', 'Program'));
  }

  public function TambahForm($idProgram){
    $idProgram = HCrypt::Decrypt($idProgram);
    $Program = Program::findOrFail($idProgram);
    return view('JadwalProgram.Tambah', compact('Program'));
  }

  public function TambahSubmit(Request $request, $idProgram){
    $idProgram = HCrypt::Decrypt($idProgram);
    $Program = Program::findOrFail($idProgram);
    $Jadwal = new JadwalProgram;
    $Jadwal->fill($request->all());
    $Jadwal->program_id = $idProgram;
    $Jadwal->save();
    return redirect()->route('programJadwalData', ['idProgram' => $Program->UUID])->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambahkan']);
  }

  public function EditForm($idProgram, $Id){
    $idProgram = HCrypt::Decrypt($idProgram);
    $Program = Program::findOrFail($idProgram);
    $Id = HCrypt::Decrypt($Id);
    $Jadwal = JadwalProgram::findOrFail($Id);
    return view('JadwalProgram.Edit', compact('Jadwal', 'Program'));
  }

  public function EditSubmit(Request $request, $idProgram, $Id){
    $idProgram = HCrypt::Decrypt($idProgram);
    $Program = Program::findOrFail($idProgram);
    $Id = HCrypt::Decrypt($Id);
    $Jadwal = JadwalProgram::findOrFail($Id);
    $Jadwal->fill($request->all());
    $Jadwal->save();
    return redirect()->route('programJadwalData', ['idProgram' => $Program->UUID])->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ubah']);
  }

  public function Hapus($idProgram, $verify=null, $Id=null){
    if ($verify) {
      $idProgram = HCrypt::Decrypt($idProgram);
      $Program = Program::findOrFail($idProgram);
      $Id = HCrypt::Decrypt($Id);
      $Jadwal = JadwalProgram::findOrFail($Id);
      $Jadwal->delete();
      return redirect()->route('programJadwalData', ['idProgram' => $Program->UUID])->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Hapus']);
    }
    return abort(404);
  }
}
