<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Keuangan;

class KeuanganController extends Controller
{
  public function Data(){
    $dataKeuangan = Keuangan::orderBy('id', 'desc')->get();
    $Keuangan = $dataKeuangan->map(function ($data){
      if ($data->sumber == 1) {
        if ($data->Pendaftaran->PendaftaranProgram->first()->karyawan_id != 0) return $data;
      }else return $data;
    })->reject(function ($data) {
      return empty($data);
    });
    return view('Keuangan.Data', compact('Keuangan'));
  }
}
