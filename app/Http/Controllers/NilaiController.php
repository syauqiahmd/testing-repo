<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PendaftaranProgram;
use App\Pendaftaran;
use App\Nilai;
use HCrypt;

class NilaiController extends Controller
{
  public function Data(){
    $Pendaftaran = Pendaftaran::whereHas('PendaftaranProgram', function ($query) {
      $query->where('karyawan_id', '!=', 0);
    })->get();
    return view('Nilai.Data', compact('Pendaftaran'));
  }

  public function Detail($Id){
    $Id = HCrypt::Decrypt($Id);
    $Pendaftaran = Pendaftaran::findOrFail($Id);
    return view('Nilai.Detail', compact('Pendaftaran'));
  }

  public function Edit($Id){
    $Id = HCrypt::Decrypt($Id);
    $PendaftaranProgram = PendaftaranProgram::findOrFail($Id);
    return view('Nilai.Edit', compact('PendaftaranProgram'));
  }

  public function EditSubmit(Request $request, $Id){
    $Id = HCrypt::Decrypt($Id);
    $PendaftaranProgram = PendaftaranProgram::findOrFail($Id);
    foreach ($request->materi as $index=>$dataRequest) {
      $Nilai = Nilai::wherePendaftaranProgramId($PendaftaranProgram->id)->whereMateriId($index)->first()??new Nilai;
      $Nilai->nilai = $dataRequest;
      $Nilai->materi_id = $index;
      $Nilai->pendaftaran_program_id = $PendaftaranProgram->id;
      $Nilai->save();
    }
    return redirect()->route('nilaiDetail', ['id' => $PendaftaranProgram->Pendaftaran->UUID])->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Nilai Berhasil Diubah']);
  }
}
