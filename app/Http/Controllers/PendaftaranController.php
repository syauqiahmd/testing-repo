<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PendaftaranProgram;
use App\Pendaftaran;
use Carbon\Carbon;
use App\Keuangan;
use App\Karyawan;
use App\Peserta;
use App\Program;
use HCrypt;

class PendaftaranController extends Controller
{
  public function Data(){
    $Pendaftaran = Pendaftaran::whereHas('PendaftaranProgram', function ($query) {
      $query->where('karyawan_id', '!=', 0);
    })->get();
    return view('Pendaftaran.Data', compact('Pendaftaran'));
  }

  public function Info($Id){
    $Id = HCrypt::Decrypt($Id);
    $Pendaftaran = Pendaftaran::findOrFail($Id);
    return view('Pendaftaran.Info', compact('Pendaftaran'));
  }

  public function TambahForm(){
    $Peserta = Peserta::all();
    $Program = Program::has('JadwalProgram')->get();
    return view('Pendaftaran.Tambah', compact('Peserta', 'Program'));
  }

  public function TambahSubmit(Request $request){
    if (!$request->program_id) {
      return back()->withInput()->with(['alert' => true, 'tipe' => 'error', 'judul' => 'Ada Kesalahan', 'pesan' => 'Harap Isi Semua Data']);
    }
    $Pendaftaran = new Pendaftaran;
    $Pendaftaran->fill($request->all());
    $Pendaftaran->save();
    $Pendaftaran->Program()->attach($request->program_id);
    $Keuangan = new Keuangan;
    $Keuangan->tipe = 1;
    $Keuangan->sumber = 1;
    $Keuangan->jumlah = $request->bayar;
    $Keuangan->pendaftaran_id = $Pendaftaran->id;
    $Keuangan->save();
    return redirect()->route('pendaftaranJadwalForm', ['id' => $Pendaftaran->UUID]);
  }

  public function JadwalForm($Id){
    $Id = HCrypt::Decrypt($Id);
    $Pendaftaran = Pendaftaran::findOrFail($Id);
    $PendaftaranProgram = PendaftaranProgram::wherePendaftaranId($Id)->get();
    $Karyawan = Karyawan::all();
    return view('Pendaftaran.Jadwal', compact('Pendaftaran','PendaftaranProgram','Karyawan'));
  }

  public function JadwalSubmit(Request $request, $Id){
    $Id = HCrypt::Decrypt($Id);
    $PendaftaranProgram = PendaftaranProgram::wherePendaftaranId($Id)->get();
    foreach ($PendaftaranProgram as $Data) {
      $Data->JadwalProgram()->attach($request->jadwal_program_id[$Data->id]);
      $Data->karyawan_id = $request->karyawan_id[$Data->id];
      $Data->save();
    }
    return redirect()->route('pendaftaranData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambah']);
  }

  public function Hapus($Verify, $Id){
    if ($Verify) {
      $Id = HCrypt::Decrypt($Id);
      $Pendaftaran = Pendaftaran::findOrFail($Id);
      $PendaftaranProgram = PendaftaranProgram::wherePendaftaranId($Id)->get();
      foreach ($PendaftaranProgram as $Data) {
        $Data->JadwalProgram()->detach();
      }
      $Pendaftaran->Program()->detach();
      $Pendaftaran->Keuangan()->delete();
      $Pendaftaran->delete();
      return redirect()->route('pendaftaranData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Dihapus']);
    }
    return abort(404);
  }
}
