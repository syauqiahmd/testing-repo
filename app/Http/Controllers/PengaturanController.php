<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengaturan;
use App\Karyawan;

class PengaturanController extends Controller
{
  public function pengaturan(){
    $karyawan = Karyawan::all();
    $pengaturan = Pengaturan::first();
    return view('pengaturan.pengaturan', compact('karyawan', 'pengaturan'));
  }

  public function pengaturanSubmit(Request $request){
    $pengaturan = Pengaturan::count()? Pengaturan::first() : new Pengaturan;
    $pengaturan->fill($request->all());
    $pengaturan->save();
    return redirect()->route('pengaturan')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Berhasil Menyimpan']);
  }
}
