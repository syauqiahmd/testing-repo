<?php

namespace App\Http\Controllers;

use App\Exports\KeuanganSpecificExport;
use App\Exports\KaryawansExport;
use App\Exports\KeuangansExport;
use App\Exports\ProgramsExport;
use App\Exports\PesertaExport;
use App\Exports\NilaisExport;
use Illuminate\Http\Request;
use Excel;

class ExportController extends Controller
{
  public function peserta(Request $request){
    $data = decrypt($request->peserta);
    return Excel::download(new PesertaExport($data), 'Peserta.xlsx');
  }

  public function program(Request $request){
    $data = decrypt($request->program);
    return Excel::download(new ProgramsExport($data), 'Program.xlsx');
  }

  public function nilai(Request $request){
    $data = decrypt($request->nilai);
    return Excel::download(new NilaisExport($data), 'Nilai.xlsx');
  }

  public function karyawan(Request $request){
    $data = decrypt($request->karyawan);
    return Excel::download(new KaryawansExport($data), 'Karyawan.xlsx');
  }

  public function keuangan(Request $request){
    $data = decrypt($request->keuangan);
    return Excel::download(new KeuangansExport($data), 'Keuangan.xlsx');
  }

  public function keuanganSpecific(Request $request, $tipe){
    $data = decrypt($request->keuangan);
    return Excel::download(new KeuanganSpecificExport($data), "Keuangan $tipe.xlsx");
  }
}
