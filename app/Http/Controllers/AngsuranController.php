<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendaftaran;
use App\Keuangan;
use HCrypt;

class AngsuranController extends Controller
{
  public function Data(){
    $Pendaftaran = Pendaftaran::whereHas('PendaftaranProgram', function ($query) {
      $query->where('karyawan_id', '!=', 0);
    })->get()->sortBy('StatusBayar');
    return view('Keuangan.Angsuran.Data', compact('Pendaftaran'));
  }

  public function Info($Id){
    $Id = HCrypt::Decrypt($Id);
    $Pendaftaran = Pendaftaran::findOrFail($Id);
    return view('Keuangan.Angsuran.Info', compact('Pendaftaran'));
  }

  public function Bayar($Id){
    $Id = HCrypt::Decrypt($Id);
    $Pendaftaran = Pendaftaran::findOrFail($Id);
    return view('Keuangan.Angsuran.Bayar', compact('Pendaftaran'));
  }

  public function BayarSubmit(Request $request, $Id){
    $Id = HCrypt::Decrypt($Id);
    $Keuangan = new Keuangan;
    $Keuangan->tipe = 1;
    $Keuangan->sumber = 1;
    $Keuangan->pendaftaran_id = $Id;
    $Keuangan->fill($request->all());
    $Keuangan->save();
    return redirect()->route('keuanganAngsuranData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambahkan']);
  }
}
