<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Materi;
use HCrypt;

class ProgramController extends Controller
{
  public function Data(){
    $Program = Program::all();
    return view('Program.Data', compact('Program'));
  }

  public function TambahForm(){
    return view('Program.Tambah');
  }

  public function TambahSubmit(Request $request){
    $Program = new Program;
    $Program->fill($request->all());
    $Program->save();
    foreach (array_filter($request->materi) as $materi) {
      $Materi = new Materi;
      $Materi->nama = $materi;
      $Materi->program_id = $Program->id;
      $Materi->save();
    }
    return redirect()->route('programData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Ditambahkan']);
  }

  public function EditForm($Id){
    $Id = HCrypt::Decrypt($Id);
    $Program = Program::findOrFail($Id);
    return view('Program.Edit', compact('Program'));
  }

  public function EditSubmit(Request $request, $Id){
    $Id = HCrypt::Decrypt($Id);
    $Program = Program::findOrFail($Id);
    $Program->fill($request->all());
    $Program->save();
    return redirect()->route('programData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Diubah']);
  }

  public function Hapus($Verify=null, $Id=null){
    if ($Verify) {
      $Id = HCrypt::Decrypt($Id);
      $Program = Program::findOrFail($Id);
      $Program->delete();
      return redirect()->route('programData')->with(['alert' => true, 'tipe' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Data Berhasil Dihapus']);
    }
  }
}
