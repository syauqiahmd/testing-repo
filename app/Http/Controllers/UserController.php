<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
  public function Data($Tipe){
    $Tipe = strtolower($Tipe);
    if ($Tipe = 'karyawan') {
      $User = User::whereTipe(1)->get();
    }elseif ($Tipe = 'peserta') {
      $User = User::whereTipe(2)->get();
    }
    return view('User.Data', compact('User','Tipe'));
  }
}
