<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PendaftaranProgram;
use App\Pendaftaran;
use App\Pengaturan;
use App\Karyawan;
use App\Keuangan;
use App\Peserta;
use App\Program;
use HDefault;
use HCrypt;
use PDF;

class LaporanController extends Controller
{
  public function Peserta(){
    $Peserta = Peserta::all();
    return view('Laporan.Peserta', compact('Peserta'));
  }

  public function cetakPeserta(){
    $Peserta = Peserta::all();
    $pdf = PDF::loadview('Cetak.Peserta', compact('Peserta'));
    return $pdf->setPaper('a4', 'potrait')->stream();
  }

  public function pendaftaran(){
    $Pendaftaran = Pendaftaran::whereHas('PendaftaranProgram', function ($query) {
      $query->where('karyawan_id', '!=', 0);
    })->get();
    return view('Laporan.pendaftaran', compact('Pendaftaran'));
  }

  public function cetakPendaftaran(){
    $Pendaftaran = Pendaftaran::whereHas('PendaftaranProgram', function ($query) {
      $query->where('karyawan_id', '!=', 0);
    })->get();
    $pdf = PDF::loadview('Cetak.pendaftaran', compact('Pendaftaran'));
    return $pdf->setPaper('a4', 'potrait')->stream();
  }

  public function Program(){
    $Program = Program::all();
    return view('Laporan.Program', compact('Program'));
  }

  public function cetakProgram(){
    $Program = Program::all();
    $pdf = PDF::loadview('Cetak.Program', compact('Program'));
    return $pdf->setPaper('a4', 'potrait')->stream();
  }

  public function NilaiPeserta(){
    $PendaftaranProgram = PendaftaranProgram::all();
    return view('Laporan.NilaiPeserta', compact('PendaftaranProgram'));
  }

  public function cetakNilaiPeserta(){
    $PendaftaranProgram = PendaftaranProgram::all();
    $pdf = PDF::loadview('Cetak.NilaiPeserta', compact('PendaftaranProgram'));
    return $pdf->setPaper('a4', 'potrait')->stream();
  }

  public function Karyawan(){
    $Karyawan = Karyawan::all();
    return view('Laporan.Karyawan', compact('Karyawan'));
  }

  public function cetakKaryawan(){
    $Karyawan = Karyawan::all();
    $pdf = PDF::loadview('Cetak.Karyawan', compact('Karyawan'));
    return $pdf->setPaper('a4', 'potrait')->stream();
  }

  public function Keuangan(Request $request){
    $dataKeuangan = Keuangan::orderBy('id', 'desc');
    if ($request->all()) $dataKeuangan->whereBetween('created_at', [$request->tanggal_awal, $request->tanggal_akhir]);
    $Keuangan = $dataKeuangan->get()->map(function ($data){
      if ($data->sumber == 1) {
        if ($data->Pendaftaran->PendaftaranProgram->first()->karyawan_id != 0) return $data;
      }else return $data;
    })->reject(function ($data) {
      return empty($data);
    });
    return view('Laporan.Keuangan', compact('Keuangan', 'request'));
  }

  public function cetakKeuangan(Request $request){
    $Keuangan = decrypt($request->keuangan);
    $pdf = PDF::loadview('Cetak.Keuangan', compact('Keuangan'));
    return $pdf->setPaper('a4', 'potrait')->stream();
  }

  public function keuanganSpecific(Request $request, $tipe){
    $sumber = HDefault::sumberKeuangan($tipe);
    if (!$sumber) return abort(404);
    $dataKeuangan = Keuangan::whereSumber($sumber)->orderBy('id', 'desc');
    if ($request->all()) $dataKeuangan->whereBetween('created_at', [$request->tanggal_awal, $request->tanggal_akhir]);
    $keuangan = $dataKeuangan->get()->map(function ($data){
      if ($data->sumber == 1) {
        if ($data->Pendaftaran->PendaftaranProgram->first()->karyawan_id != 0) return $data;
      }else return $data;
    })->reject(function ($data) {
      return empty($data);
    });
    return view('Laporan.keuanganSpecific', compact('keuangan', 'tipe', 'request'));
  }

  public function cetakKeuanganSpecific(Request $request, $tipe){
    $keuangan = decrypt($request->keuangan);
    $pdf = PDF::loadview('Cetak.keuanganSpecific', compact('keuangan', 'tipe'));
    return $pdf->setPaper('a4', 'potrait')->stream();
  }

  public function Sertifikat($type = 'Sertifikat'){
    $Pendaftaran = Pendaftaran::whereHas('PendaftaranProgram', function ($query) {
      $query->where('karyawan_id', '!=', 0);
    })->get();
    return view('Laporan.Sertifikat', compact('Pendaftaran', 'type'));
  }

  public function cetakSertifikat($id, $type = 'Sertifikat'){
    $id = HCrypt::Decrypt($id);
    $Pendaftaran = Pendaftaran::findOrFail($id);
    $pengaturan = Pengaturan::first();
    $pdf = PDF::loadview("Cetak.sertifikat.$type", compact('Pendaftaran', 'pengaturan'));
    return $pdf->setPaper('a4', 'landscape')->stream();
  }
}
