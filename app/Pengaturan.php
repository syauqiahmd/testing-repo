<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengaturan extends Model
{
  protected $fillable = ['nama', 'alamat', 'karyawan_id', 'tentang', 'kontak', 'syarat'];

  public function Karyawan(){
    return $this->belongsTo('App\Karyawan');
  }
}
