<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use HTanggal;
use HCrypt;

class JadwalProgram extends Model
{
  use SoftDeletes;

  protected $fillable = ['hari', 'jam_awal', 'jam_akhir'];

  public function Program(){
    return $this->belongsTo('App\Program');
  }

  public function getJamAttribute(){
    return "{$this->jam_awal} - {$this->jam_akhir}";
  }

  public function getJadwalSelanjutnyaAttribute(){
    $Hari = HTanggal::FormatHariEN($this->hari);
    return HTanggal::FormatDate(new Carbon("next {$Hari}"));
  }

  public function getUUIDAttribute(){
    return HCrypt::Encrypt($this->id);
  }
}
