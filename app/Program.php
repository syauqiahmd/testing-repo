<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use HCrypt;

class Program extends Model
{
  use SoftDeletes;

  protected $fillable = ['nama', 'jumlah_pertemuan', 'biaya'];

  public function getUUIDAttribute($value){
    return HCrypt::Encrypt($this->id);
  }

  public function JadwalProgram(){
    return $this->hasMany('App\JadwalProgram');
  }

  public function Materi(){
    return $this->hasMany('App\Materi');
  }

  public function Pendaftaran(){
    return $this->belongsToMany('App\Pendaftaran');
  }
}
