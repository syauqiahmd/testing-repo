<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use HCrypt;

class PendaftaranProgram extends Model
{
  protected $table = 'pendaftaran_program';

  protected $fillable = ['nilai'];

  public function getUUIDAttribute(){
    return HCrypt::Encrypt($this->id);
  }

  public function Pendaftaran(){
    return $this->belongsTo('App\Pendaftaran');
  }

  public function PendaftaranProgramJadwalProgram(){
    return $this->hasMany('App\PendaftaranProgramJadwalProgram');
  }

  public function Nilai(){
    return $this->hasMany('App\Nilai');
  }

  public function Program(){
    return $this->belongsTo('App\Program');
  }

  public function Karyawan(){
    return $this->belongsTo('App\Karyawan');
  }

  public function JadwalProgram(){
    return $this->belongsToMany('App\JadwalProgram', 'pendaftaran_program_jadwal_program');
  }
}
