<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use HCrypt;

class Jabatan extends Model
{
  use SoftDeletes;

  protected $fillable = ['nama'];

  public function getUUIDAttribute($value){
    return HCrypt::Encrypt($this->id);
  }
}
