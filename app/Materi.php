<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materi extends Model
{
  use SoftDeletes;
  
  protected $fillable = ['nama', 'keterangan', 'program_id'];

  public function Program(){
    return $this->belongsTo('App\Program');
  }

  public function getUUIDAttribute(){
    return encrypt($this->id);
  }
}
