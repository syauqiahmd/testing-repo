<?php
namespace App\Helpers;

class DefaultHelper
{
  public static function sumberKeuangan($value = null){
    $data = [
      1 => 'Angsuran',
      2 => 'Jasa',
      3 => 'Operasional',
      4 => 'Gaji'
    ];

    if ($value) return array_search(ucwords($value), $data);
    return $data;
  }
}
