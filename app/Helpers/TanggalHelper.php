<?php
namespace App\Helpers;

Use Carbon\Carbon;
Use HTanggal;

class TanggalHelper
{
  private static function FormatMonth($value){
    $Month = [
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember',
    ];
    return $Month[$value-1];
  }

  public static function FormatDate($Date){
    $Tanggal = Carbon::Parse($Date)->format('d');
    $Bulan = HTanggal::FormatMonth(Carbon::Parse($Date)->format('m'));
    $Tahun = Carbon::Parse($Date)->format('Y');
    return "{$Tanggal} {$Bulan} {$Tahun}";
  }

  public static function FormatDateTime($Data){
    return Carbon::Parse($Data)->format('d-m-Y H:i a');
  }

  public static function FormatHariID($value=null){
    $Hari = [
      'Senin',
      'Selasa',
      'Rabu',
      'Kamis',
      'Jumat',
      'Sabtu',
      'Minggu',
    ];
    if ($value) {
      return $Hari[$value-1];
    }
    return $Hari;
  }

  public static function FormatHariEN($value=null){
    $Hari = [
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday',
    ];
    return $Hari[$value-1];
  }

  public static function Now(){
    return Carbon::now()->format('Y-m-d');
  }

  public static function PassDate($value1, $value2=null){
    if (!$value2) $value2 = Carbon::now();
    return $value1->lessThanOrEqualTo($value2)? true:false;
  }

  public static function EqualDate($value1, $value2){
    $value1 = Carbon::parse($value1);
    $value2 = Carbon::parse($value2);
    return $value1->equalTo($value2)? true:false;
  }

  public static function PeriodeText($value){
    $date = Carbon::parse($value);
    $MonthFormat = HTanggal::FormatMonth($date->format('m'));
    return "{$MonthFormat} {$date->format("Y")}";
  }

  public static function Periode(){
    $now = Carbon::now()->firstOfMonth()->subMonths(12);
    for ($i=0; $i < 24; $i++) {
      $periode = $now->copy()->addMonths($i);
      $return[$i]['date'] = $periode->format('Y-m-d');
      $return[$i]['text'] = HTanggal::PeriodeText($periode);
    }
    return collect($return);
  }
}
