<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendaftaranProgramJadwalProgram extends Model
{
  protected $table = 'pendaftaran_program_jadwal_program';

  public function JadwalProgram(){
    return $this->belongsTo('App\JadwalProgram');
  }
}
