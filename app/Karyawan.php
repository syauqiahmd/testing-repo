<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use HTanggal;
use HCrypt;

class Karyawan extends Model
{
  use SoftDeletes;

  protected $fillable = ['nama', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'no_handphone', 'gaji', 'jabatan_id', 'user_id'];

  public function User(){
    return $this->belongsTo('App\User');
  }

  public function Jabatan(){
    return $this->belongsTo('App\Jabatan');
  }

  public function Keuangan(){
    return $this->hasMany('App\Keuangan');
  }

  public function getTTLAttribute(){
    $Tanggal = HTanggal::FormatDate($this->tanggal_lahir);
    return "{$this->tempat_lahir}, {$Tanggal}";
  }

  public function getUUIDAttribute(){
    return HCrypt::Encrypt($this->id);
  }

  public function getStatusGajiAttribute(){
    if ($this->Keuangan->count()) {
      return $this->Keuangan->last()->created_at->firstOfMonth()->eq(Carbon::now()->firstOfMonth());
    }
    return false;
  }

  public function getStatusGajiTextAttribute(){
    if ($this->StatusGaji) {
      return "Sudah Dibayarkan";
    }
    return "Belum Dibayarkan";
  }
}
