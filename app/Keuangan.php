<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use HCrypt;

class Keuangan extends Model
{
  protected $fillable = ['jumlah', 'keterangan', 'karyawan_id', 'periode_gaji'];

  public function getUUIDAttribute(){
    return HCrypt::Encrypt($this->id);
  }

  public function Karyawan(){
    return $this->belongsTo('App\Karyawan');
  }

  public function Pendaftaran(){
    return $this->belongsTo('App\Pendaftaran');
  }

  public function getTipeTextAttribute(){
    switch ($this->tipe) {
      case 1:
        return "Pemasukan";
        break;
      case 2:
        return "Pengeluaran";
        break;
      default:
        return "Kode Salah";
        break;
    }
  }

  public function getSumberTextAttribute(){
    switch ($this->sumber) {
      case 1:
        return "Angsuran Kursus";
        break;
      case 2:
        return "Jasa";
        break;
      case 3:
        return "Operasional";
        break;
      case 4:
        return "Gaji";
        break;

      default:
        return "Kode Salah";
        break;
    }
  }

  public function getKeteranganTextAttribute(){
    switch ($this->sumber) {
      case 1:
        return "Pendaftaran #{$this->Pendaftaran->id}";
        break;
      case 2:
        return $this->keterangan;
        break;
      case 3:
        return $this->keterangan;
        break;
      case 4:
        return "Gaji Karyawan {$this->Karyawan->nama}";
        break;

      default:
        return "Kode Salah";
        break;
    }
  }
}
