<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use SoftDeletes;
  use Notifiable;

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'username', 'password', 'tipe',
  ];

  /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
  protected $hidden = [
    'password', 'remember_token',
  ];

  public function Karyawan(){
    return $this->hasOne('App\Karyawan');
  }

  public function Peserta(){
    return $this->hasOne('App\Peserta');
  }

  public function getDataAttribute($value){
    if ($this->tipe == 1) {
      return $this->Karyawan;
    }elseif ($this->tipe == 2) {
      return $this->Peserta;
    }
  }

  public function getTipeTextAttribute($value){
    switch ($this->tipe) {
      case 1:
        $return = 'Karyawan';
        break;
      case 2:
        $return = 'Peserta';
        break;

      default:
        $return = 'GOD';
        break;
    }
    return $return;
  }

  public function setPasswordAttribute($value){
    if ($value) {
      $this->attributes['password'] = bcrypt($value);
    }
  }
}
