<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use HCrypt;

class Pendaftaran extends Model
{
  protected $fillable = ['peserta_id', 'total_bayar', 'tanggal_daftar'];

  public function Peserta(){
    return $this->belongsTo('App\Peserta');
  }

  public function Program(){
    return $this->belongsToMany('App\Program');
  }

  public function Keuangan(){
    return $this->hasMany('App\Keuangan');
  }

  public function PendaftaranProgram(){
    return $this->hasMany('App\PendaftaranProgram');
  }

  public function getUUIDAttribute(){
    return HCrypt::Encrypt($this->id);
  }

  public function getTotalDibayarAttribute(){
    return $this->Keuangan->sum('jumlah');
  }

  public function getSisaBayarAttribute(){
    return $this->total_bayar - $this->TotalDibayar;
  }


  public function getAllJadwalAttribute(){
    $i = 1;
    $selectedJadwal = $this->PendaftaranProgram->pluck('PendaftaranProgramJadwalProgram')->flatten()->pluck('JadwalProgram')->groupBy('program_id');
    $selectedProgram = $this->PendaftaranProgram;
    $tanggalMasuk = Carbon::parse($this->tanggal_daftar);
    foreach ($selectedProgram as $Program) {
      $a = 1;
      $loop = 0;
      $jumlahPertemuan = $Program->Program->jumlah_pertemuan;
      do {
        foreach ($selectedJadwal[$Program->program_id] as $Data) {
          if ($a <= $jumlahPertemuan) {
            $WaktuSelesai = Carbon::parse($Data->jam_akhir);
            $Jam = $WaktuSelesai->Format("H");
            $Menit = $WaktuSelesai->Format("i");
            $Detik = $WaktuSelesai->Format("s");
            $jadwalMasuk = $tanggalMasuk->copy()->startOfWeek()->addDays("{$Data->hari}")->subDay();
            $mingguKe = $jadwalMasuk < $tanggalMasuk? 7*($loop+1): $mingguKe = 7*$loop;
            $jadwalMasuk = $jadwalMasuk->addDays("{$mingguKe}")->addHours($Jam)->addMinutes($Menit)->addSeconds($Detik);
            $return[$i]['Jadwal'] = $jadwalMasuk;
            $return[$i]['Data'] = $Data;
            $i++;
          }
          $a++;
        }
        $loop++;
      } while ($a <= $jumlahPertemuan);
    }
    return collect($return);
  }

  public function getStatusKursusAttribute(){
    $jadwalTerakhir = $this->AllJadwal->max('Jadwal');
    return $jadwalTerakhir<Carbon::now()? 'Telah Berakhir' : 'Sedang Berlangsung';
  }

  public function getStatusBayarAttribute(){
    if ($this->SisaBayar<=0) {
      return "Lunas";
    }
    return "Belum Lunas";
  }
}
